@extends('layouts.app')

@section('title')
Sales List
@endsection

@section('breadcrumb')
@parent
<li>Sales</li>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
			<div class="box-header with-border bg-warning">
				<span>Date Filter</span>
			</div>
            <div class="box-body">
                <form action="{{route('sales.index')}}" method="GET" id="form-filer">
                    <div class='col-md-4'>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' id="date1" class="form-control" name="date1"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class='col-md-4'>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker2'>
                                <input type='text' id="date2" class="form-control" name="date2"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-success" onclick="filterData()">
                            <span class="glyphicon glyphicon-filter"></span>
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <div class="box">
            <div class="box-body">
                <table class="table table-striped tabel-penjualan">
                    <thead>
                        <tr>
                            <th width="30">No</th>
                            <th>Date</th>
                            <th>Receipt No.</th>
                            <th>Total Items</th>
                            <th>Total Amount</th>
                            <th>Discount</th>
                            <th>Payment</th>
                            <th>Branch</th>
                            <th>Cashier</th>
                            <th width="100">Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>

            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        Top 10 Products
                    </div>
                    <div class="box-body">
                        {!! $productChart->render() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        Top 10 Categories
                    </div>
                    <div class="box-body">
                        {!! $categoryChart->render() !!}
                    </div>
                </div>
            </div>
        </div>
        

        <div class="box">
            <div class="box">
                <div class="box-header with-border">
                    Returned Items
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped tabel-return">
                    <thead>
                        <tr>
                            <th width="30">No</th>
                            <th>Date</th>
                            <th>Receipt No.</th>
                            <th>Return QTY</th>
                            <th>Return Amount</th>
                            <th>Branch</th>
                            <th>Cashier</th>
                            <th>Reason</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>

            </div>
        </div>
    </div>
</div>

@include('sales.detail')
@endsection

@section('script')
<script type="text/javascript">
    var table, save_method, table1, table2;
    $(function () {
        $('#datetimepicker1').datepicker();
        $('#datetimepicker2').datepicker({
            useCurrent: false
        });
        $('#datetimepicker1').datepicker( "setDate", "{{ date('m/d/Y', strtotime($o_date1)) }}");
        $('#datetimepicker2').datepicker( "setDate", "{{ date('m/d/Y', strtotime($o_date2)) }}");

        $("#datetimepicker1").on("dp.change", function (e) {
            $('#datetimepicker2').data("datepicker").minDate(e.date);
        });

        $("#datetimepicker2").on("dp.change", function (e) {
            $('#datetimepicker1').data("datepicker").maxDate(e.date);
        });

        table = $('.tabel-penjualan').DataTable({
            "processing": true,
            "serverside": true,
            "ajax": {
                "url": "{{ route('sales.data') }}",
                "type": "GET",
                "data": {
                    "date1": $('#date1').val(),
                    "date2": $('#date2').val(),
                }
            }
        });

        table1 = $('.tabel-detail').DataTable({
            "dom": 'Brt',
            "bSort": false,
            "processing": true
        });

        table2 = $('.tabel-return').DataTable({
            "dom": 'Brt',
            "bSort": false,
            "processing": true,
            "ajax": {
                "url": "{{ route('sales.dataReturn') }}",
                "type": "GET",
                "data": {
                    "date1": $('#date1').val(),
                    "date2": $('#date2').val(),
                }
            }
        });

        $('.tabel-supplier').DataTable();
    });

    function addForm() {
        $('#modal-supplier').modal('show');
    }

    function showDetail(id) {
        $('#modal-detail').modal('show');

        table1.ajax.url("sales/" + id + "/show");
        table1.ajax.reload();
    }

    function deleteData(id) {
        if (confirm("Are you sure you want to delete trasaction?")) {
            $.ajax({
                url: "sales/" + id,
                type: "POST",
                data: {
                    '_method': 'DELETE',
                    '_token': $('meta[name=csrf-token]').attr('content')
                },
                success: function (data) {
                    table.ajax.reload();
                },
                error: function () {
                    alert("Error in delete");
                }
            });
        }
    }

</script>
@endsection
