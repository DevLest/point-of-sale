@extends('layouts.app')

@section('title')
  Member List
@endsection

@section('breadcrumb')
   @parent
   <li>member</li>
@endsection

@section('content')     
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add New Member</a>
        <a onclick="printCard()" class="btn btn-info"><i class="fa fa-credit-card"></i> Print Card</a>
      </div>
      <div class="box-body"> 

<form method="post" id="form-member">
{{ csrf_field() }}
<table class="table table-striped">
<thead>
   <tr>
      <th width="20"><input type="checkbox" value="1" id="select-all"></th>
      <th width="20">No</th>
      <th>Member Code</th>
      <th>Name</th>
      <th>Address</th>
      <th>Phone</th>
      <th width="100">Action</th>
   </tr>
</thead>
<tbody></tbody>
</table>
</form>

      </div>
    </div>
  </div>
</div>

@include('member.form')
@endsection

@section('script')
<script type="text/javascript">
var table, save_method;
$(function(){
   table = $('.table').DataTable({
     "processing" : true,
     "ajax" : {
       "url" : "{{ route('member.data') }}",
       "type" : "GET"
     },
     'columnDefs': [{
         'targets': 0,
         'searchable': false,
         'orderable': false
      }],
      'order': [1, 'asc']
   }); 

   $('#select-all').click(function(){
      $('input[type="checkbox"]').prop('checked', this.checked);
   });
   
   $('#modal-form form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
         var id = $('#id').val();
         if(save_method == "add") url = "{{ route('member.store') }}";
         else url = "member/"+id;
         
         $.ajax({
           url : url,
           type : "POST",
           data : $('#modal-form form').serialize(),
           dataType: 'JSON',
           success : function(data){
            if(data.msg=="error"){
              alert('Saving Error');
              $('#kode').focus().select();
            }else{
              $('#modal-form').modal('hide');
              table.ajax.reload();
            }
           },
           error : function(){
             alert("Error is saving");
           }   
         });
         return false;
     }
   });
});

function addForm(){
   save_method = "add";
   $('input[name=_method]').val('POST');
   $('#modal-form').modal('show');
   $('#modal-form form')[0].reset();            
   $('.modal-title').text('Add Member');
   $('#code').attr('readonly', false);
}

function editForm(id){
   save_method = "edit";
   $('input[name=_method]').val('PATCH');
   $('#modal-form form')[0].reset();
   $.ajax({
     url : "member/"+id+"/edit",
     type : "GET",
     dataType : "JSON",
     success : function(data){
       $('#modal-form').modal('show');
       $('.modal-title').text('Edit Member');
       
       $('#id').val(data.member_id);
       $('#code').val(data.member_code).attr('readonly', true);
       $('#name').val(data.name);
       $('#address').val(data.address);
       $('#phone').val(data.phone);
       
     },
     error : function(){
       alert("Error is Edit");
     }
   });
}

function deleteData(id){
   if(confirm("Are you sure you want to delete member?")){
     $.ajax({
       url : "member/"+id,
       type : "POST",
       data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
       success : function(data){
         table.ajax.reload();
       },
       error : function(){
         alert("Error in deletion of data");
       }
     });
   }
}

function printCard(){
  if($('input:checked').length < 1){
    alert('Please select data to be printed!');
  }else{
    $('#form-member').attr('target', '_blank').attr('action', "member/print").submit();
  }
}
</script>
@endsection