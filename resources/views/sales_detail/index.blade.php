@extends('layouts.app')

@section('head-style')
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 30px;
        height: 20px;
    }

    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 12px;
        width: 12px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked+.slider {
        background-color: #2196F3;
    }

    input:focus+.slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked+.slider:before {
        -webkit-transform: translateX(12px);
        -ms-transform: translateX(12px);
        transform: translateX(12px);
    }

    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }

</style>
@endsection

@section('title')
Sales Transaction
@endsection

@section('breadcrumb')
@parent
<li>Sales</li>
<li>Add</li>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                <form class="form form-horizontal form-product" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="idsales" value="{{ $idsales }}">
                    <div class="form-group">

                        <div class="alert alert-danger" id="success-alert">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <strong>ERROR! </strong> Product code not found
                        </div>

                        <label for="code" class="col-md-2 control-label">Product Code</label>
                        <div class="col-md-5">
                            <div class="input-group">
                                <input id="code"  data-toggle="tooltip" data-placement="top" title="Click the ... button on the keyboad to show all product seach panel" type="text" class="form-control" name="code" autofocus required >
                                <span class="input-group-btn">
                                    <button onclick="showProduct()" type="button" class="btn btn-info">...</button>
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-custom-product"> Custom </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>

                <form class="form-cart">
                    {{ csrf_field() }} {{ method_field('PATCH') }}
                    <table class="table table-striped table-sales table-responsive">
                        <thead>
                            <tr>
                                <th width="30">No</th>
                                <th>Product Code</th>
                                <th>Product Name</th>
                                <th align="right">Price</th>
                                <th>Total Quantity</th>
                                <th data-toggle="tooltip" data-placement="top" title="Add % sign if you want to get percentage of the sub total amount as discount otherwise add a whole number to be deducted">Discount</th>
                                <th align="right">Sub Total</th>
                                <th width="100">Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </form>

                <div class="col-md-8">
                    <div id="appear-pay"
                        style="background: #dd4b39; color: #fff; font-size: 80px; text-align: center; height: 120px">
                    </div>
                    <div id="appear-number" style="background: #3c8dbc; color: #fff; font-size: 25px; padding: 10px">
                    </div>
                </div>

                <div class="col-md-4">
                    <form class="form form-horizontal form-sales" method="post" action="transaction/save">
                        {{ csrf_field() }}
                        <input type="hidden" name="idsales" value="{{ $idsales }}">
                        <input type="hidden" name="total" id="total">
                        <input type="hidden" name="totalitem" id="totalitem">
                        <input type="hidden" name="pay" id="pay">

                        <div class="form-group">
                            <label for="totalrp" class="col-md-4 control-label">Total</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="totalrp" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="payrp" class="col-md-4 control-label">Pay</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="payrp" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="payrp" class="col-md-4 control-label">Discount</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="discount" value="0">
                            </div>
                        </div>

                        <div class="form-group" {{ ($idreturn) ? "" : "hidden"}} >
                            <label for="credit" class="col-md-4 control-label">Credit</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="credit" readonly value="{{$credit}}">
                            </div>
                        </div>

                        <div class="form-group" data-toggle="tooltip" data-placement="top" title="Double click to set exact amount as payment">
                            <label for="received" class="col-md-4 control-label">Amount Received</label>
                            <div class="col-md-8">
                                <small class="text-danger" id="changeError"></small>
                                <input type="number" class="form-control" value="0" name="received" id="received" ondblclick="exactPrice()">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="back" class="col-md-4 control-label">Change</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="back" value="0" readonly>
                            </div>
                        </div>


                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right simpan"><i
                                    class="fa fa-floppy-o"></i> Checkout</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('sales_detail.product')
@include('sales_detail.member')
@include('sales_detail.customProduct')
@endsection

@section('script')
<script type="text/javascript">
    var clickCount = 0;
    var clickIdDiscount = 0;
    var currentDiscountField = 0;
    var credit = 0;

    $(document).on('keypress', function (e) {
        if (e.which == 61) {
            showProduct();
        }
    });
    
    var table;
    $(function () {
        
        $('.table-product').DataTable();
        $("#success-alert").hide();
        $("#success-alert").hide();
        $("#adminaccess-alert").hide();

        credit = ( $('#credit').val() != "") ? $('#credit').val() : 0; 

        table = $('.table-sales').DataTable({
            "dom": 'Brt',
            "bSort": false,
            "processing": true,
            "pageLength": 100,
            "ajax": {
                "url": "{{ route('transaction.data', $idsales) }}",
                "type": "GET"
            }
        }).on('draw.dt', function () {
            loadForm();
        });

        $('.form-product').on('submit', function () {
            return false;
        });

        $('body').addClass('sidebar-collapse');

        $('#code').change(function () {
            addItem();
        });

        $('.form-cart').submit(function () {
            return false;
        });

        $('.form-sales').submit(function () {
            var received = parseFloat($('#received').val());
            var pay = parseFloat($('#pay').val());

            if (received >= pay) {
                if (confirm('Are you sure you want to checkout?')) 
                {
                    return true;
                    $('.form-sales').submit();
                }
                return false;
            }
            
            $("#changeError").html("<strong>ERROR! </strong>Please enter correct amount");
            $("#changeError").fadeTo(3000, 500).slideUp(500, function () {
                $("#changeError").slideUp(500);
            });

            return false;
        });

        $('#member').change(function () {
            selectMember($(this).val());
        });

        $('#received').change(function () {
            if ($(this).val() == "") $(this).val(0).select();
            loadForm($('#discount').val(), $(this).val());
        }).focus(function () {
            $(this).select();
        }).keyup(function () {
            if ($(this).val() == "") $(this).val(0).select();
            loadForm($('#discount').val(), $(this).val());
        });

        $('#discount').change(function () {
            if ($(this).val() == "") $(this).val(0).select();
            loadForm($(this).val(), $('#received').val());
        }).focus(function () {
            $(this).select();
        }).keyup(function () {
            if ($(this).val() == "") $(this).val(0).select();
            loadForm($(this).val(), $('#received').val());
        });

        $('.save').click(function () {
            $('.form-sales').submit();
        });

        $("#adminpassword").on('keypress',function(e) {
            if(e.which == 13) {
                $.ajax({
                    url: "transaction/admincheck",
                    type: "POST",
                    data: {
                        'password': $("#adminpassword").val(),
                        '_token': $('meta[name=csrf-token]').attr('content')
                    },
                    success: function (data)
                    {
                        if (data.status == "true") 
                        {
                            fieldId = "#discount_" + clickIdDiscount;
                            totalfield = "#total_" + clickIdDiscount;
                            wholesale = "#wholesale_" + clickIdDiscount;
                            current_discount = $(fieldId).val();
                            $(fieldId).prop("readonly", false);
                            
                            $("#adminpassword").val("");
                            $('#modal-adminpass').modal('hide');
                            $(fieldId).val("");
                            $(fieldId).focus();

                            $(fieldId).focusout(function() 
                            {
                                $(fieldId).prop("readonly", true);
                                $(fieldId).val(0);
                                clickIdDiscount = 0;

                                $(fieldId).val(current_discount);
                            }).change(function() 
                            {
                                value = $(fieldId).val()

                                if (/^\d+(\.\d+)?%$/.test(value)) {
                                    value = ( value.replace('%', '') / 100 ) * ($('#price_' + clickIdDiscount).val() * $('#total_' + clickIdDiscount).val() );
                                }
                                else
                                {
                                    value = value.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
                                }

                                // if ( wholesale >= value )
                                // {
                                loadForm((value * $(totalfield).val()), 0, clickIdDiscount);
                                location.reload();
                                // }
                                // else
                                // {
                                //     alert("Please lower down discount amount for you will lose revenue with this huge discount");
                                // }
                            });
                        } 
                        else
                        {
                            $("#adminaccess-message").html(data.message);
                            $("#adminaccess-alert").fadeTo(2000, 500).slideUp(500, function () 
                            {
                                $("#adminaccess-alert").slideUp(500);
                            });
                            $("#adminpassword").val("");
                        }
                    },
                    error: function (e) {
                        console.log(e.responseText);
                    }
                });
            }
        });

    });

    function addItem() {
        $.ajax({
            url: "{{ route('transaction.store') }}",
            type: "POST",
            data: $('.form-product').serialize(),
            success: function (data) {
                if ($.trim(data) == "Good") {
                    $('#code').val('').focus();
                    table.ajax.reload(function () {
                        loadForm($('#discount').val(), $('#received').val());
                    });
                } else {
                    $("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
                        $("#success-alert").slideUp(500);
                        $('#code').val('');
                    });
                }
            },
            error: function (e) {
                alert("Unable to save data!");
            }
        });
    }

    function showProduct() {
        $('#modal-product').modal('show');
    }

    function showMember() {
        $('#modal-member').modal('show');
    }

    function selectItem(code) {
        $('#code').val(code);
        $('#modal-product').modal('hide');
        addItem();
    }

    function changeCount(id) {
        $.ajax({
            url: "transaction/" + id,
            type: "POST",
            data: $('.form-cart').serialize(),
            success: function (data) {
                $('#code').focus();
                table.ajax.reload(function () {
                    loadForm($('#discount').val(), $('#received').val());
                });
            },
            error: function () {
                alert("Unable to save data!");
            }
        });
    }

    function changeDiscount(id) {
        if (id != clickIdDiscount)
        {
            $('#modal-adminpass').modal('show');
            $("#adminpassword").focus();
        }
        clickIdDiscount = id;
    }

    function selectMember(kode) {
        $('#modal-member').modal('hide');
        $('#member').val(kode);
        loadForm($('#discount').val(), $('#received').val());
        $('#received').val(0).focus().select();
    }

    function deleteItem(id) {
        if (confirm("Are sure the data will be deleted?")) {
            $.ajax({
                url: "transaction/" + id,
                type: "POST",
                data: {
                    '_method': 'DELETE',
                    '_token': $('meta[name=csrf-token]').attr('content')
                },
                success: function (data) {
                    table.ajax.reload(function () {
                        loadForm($('#discount').val(), $('#received').val());
                    });
                },
                error: function () {
                    alert("Cannot delete data!");
                }
            });
        }
    }

    function wholesaleprice(id) {
        clickCount++

        if (clickCount == 2) {
            $.ajax({
                url: "transaction/wholesale",
                type: "POST",
                data: {
                    'id': id,
                    '_token': $('meta[name=csrf-token]').attr('content')
                },
                success: function (data) {
                    table.ajax.reload(function () {
                        loadForm($('#discount').val(), $('#received').val());
                    });
                },
                error: function (e) {
                    console.log(e.responseText);
                    alert("Cannot delete data!");
                }
            });
            clickCount = 0;
        }
    }

    function loadForm(discount = 0, received = 0, id = 0) {
        $('#total').val(Number.isNaN($('.total').text()) ? 0 : $('.total').text());
        $('#totalitem').val(Number.isNaN($('.totalitem').text()) ? 0 : $('.totalitem').text());

        discount = parseFloat( Number.isNaN(discount) ? 0 : discount ) + parseFloat(credit);
        $received = parseFloat( Number.isNaN(received) ? 0 : received );
        
        $.ajax({
            url: "transaction/loadform/" + discount + "/" + parseFloat($('#total').val()) + "/" + received + "/" + id,
            type: "GET",
            dataType: 'JSON',
            success: function (data) {

                $('#totalrp').val("₱ " + data.totalrp);
                $('#payrp').val("₱ " + data.payrp);
                $('#pay').val(data.pay);
                $('#appear-pay').html("<small>Amount:</small> ₱ " + data.payrp);
                $('#appear-number').text(data.number);

                $('#change').val("₱ " + data.returnrip);
                $('#back').val( (data.returnirp));
                if ($('#received').val() != 0) {
                    $('#appear-pay').html("<small>Change:</small> ₱ " + data.returnirp);
                    $('#appear-number').text(data.returnnumber);
                }
            },
            error: function (e) {
                console.log(e.responseText);
                // alert("Cannot display data!");
            }
        });
    }

    function exactPrice()
    {
        $('#received').val($('#pay').val());
        loadForm($('#discount').val(), $('#pay').val());
    }

</script>

@endsection
