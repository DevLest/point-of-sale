@extends('layouts.app')

@section('head-style')
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 30px;
        height: 20px;
    }

    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 12px;
        width: 12px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked+.slider {
        background-color: #2196F3;
    }

    input:focus+.slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked+.slider:before {
        -webkit-transform: translateX(12px);
        -ms-transform: translateX(12px);
        transform: translateX(12px);
    }

    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }

</style>
@endsection

@section('title')
Return Item
@endsection

@section('breadcrumb')
@parent
<li>Sales</li>
<li>Return</li>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                {{-- <form class="form-cart" id="returnForm" action='{{route("transaction.returnSubmit")}}'> --}}
                <form class="form-cart" id="returnForm" method="POST">
                    {{ csrf_field() }}
                    <table class="table table-striped table-sales table-responsive">
                        <thead>
                            <tr>
                                <th width="30">No</th>
                                <th>Product Code</th>
                                <th>Product Name</th>
                                <th align="right">Sold Price</th>
                                <th>Sold Quantity</th>
                                <th>Discount</th>
                                <th align="right">Sub Total</th>
                                <th width="100">Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </form>

                <form action='{{route("home")}}' hidden id="redirechome"></form>
            </div>
            
            <div class="box-footer">
                <button type="button" onclick="acceptReturn()" class="btn btn-success pull-right simpan">
                    <i class="fa fa-floppy-o"></i> Accept Return
                </button> &nbsp;
                <button type="button" onclick="clearReturnTable()" class="btn btn-danger pull-right simpan">
                    <i class="fa fa-floppy-o"></i> Clear Table
                </button>
            </div>
        </div>
    </div>
</div>
@include('sales_detail.returnModal')
@endsection

@section('script')
<script type="text/javascript">
    var table;
    $(document).on('keypress', "#returnID", function (e) {
        if (e.which == 13) {
            submitId($(this).val())
        }
    });

    $("#returnID-alert").hide();

    $(function () {
        $("#returnID").focus();
        $('body').addClass('sidebar-collapse');
        
        if ("{{session('return_id')}}" === "0")
        {
            $('#modal-returnID').modal('show');

            $('#modal-returnID').on("hide.bs.modal", function () {
                
                if ("{{session('return_id')}}" === "0")
                {
                    $('#redirechome').submit();
                }
            })
        }
        else
        {
            table = $('.table-sales').DataTable({
                "dom": 'Brt',
                "bSort": false,
                "processing": true,
                "ajax": {
                    "url": "{{ route('transaction.returnData', session('return_id') ) }}",
                    "type": "GET"
                }
            }).on('draw.dt', function () {
            });
        }
    });

    function seachReturnID()
    {
        submitId($('#returnID').val())
    }

    function submitId(id)
    {
        $url = "{{ route('transaction.returnID', [ 'id' => ':id' ]) }}";
        $.ajax({
            url: $url.replace(":id", id),
            type: "POST",
            data: {
                '_token': $('meta[name=csrf-token]').attr('content')
            },
            success: function (data)
            {
                if (data.status == "true") 
                {
                    $('#modal-returnID').modal('hide');
                    location.reload();
                } 
                else
                {
                    $("#returnID-message").html(data.message);
                    $("#returnID-alert").fadeTo(4000, 500).slideUp(500, function () 
                    {
                        $("#returnID-alert").slideUp(500);
                    });
                    $("#returnID").val("");
                    $("#returnID").focus();
                }
            },
            error: function (e) {
                console.log(e.responseText);
            }
        });
    }

    function clearReturnTable()
    {
        val = "{{ session(['return_id' => 0]) }}";
        location.reload();
    }

    function acceptReturn()
    {
        $.ajax({
            type: "POST",
            url: "{{ route('transaction.returnConfirm') }}",
            data: $('#returnForm').serialize(),
            success: function (output)
            {
                $('#modal-return').modal('show');
                $('#modal-return-body').html(output);
            },
            error: function (e) {
                console.log(e.responseText);
            }
        });
    }
</script>

@endsection
