<div class="modal" id="modal-product" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true"> &times; </span> </button>
                <h3 class="modal-title">
                    Search for Products</h3>
            </div>

            <div class="modal-body">
                <table class="table table-striped table-product">
                    <thead>
                        <tr>
                            <th>Product Code</th>
                            <th>Product name</th>
                            <th>In-Stock</th>
                            <th>Price</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($product as $data)
                            <tr>
                                <th>{{ $data->product_code }}</th>
                                <th>{{ $data->product_name }}</th>
                                <th>{{ $data->stock }}</th>
                                <th>PhP {{ format_money($data->price) }}</th>
                                <th>
                                    <a onclick="selectItem('{{ $data->product_code }}')" class="btn btn-primary">
                                        <i class="fa fa-check-circle"></i>
                                        Select
                                    </a>
                                </th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>

        </div>
    </div>
</div>

<div class="modal" id="modal-adminpass" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-m">
        <div class="modal-content" style="text-align:center;">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true"> &times; </span> </button>
                <h3 class="modal-title">
                    Please enter admin password</h3>
            </div>

            <div class="modal-body">

                <div class="alert alert-danger" id="adminaccess-alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>ERROR! </strong> <div id="adminaccess-message"></div>
                </div>
                <input type="password" style="text-align:center;" class="form-control" name="adminpassword" id="adminpassword">
            </div>

        </div>
    </div>
</div>
