<div class="modal" id="modal-custom-product" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-xl">
        <div class="modal-content" style="text-align:center;">
            <form method="POST" id="modal-custom-form" action="{{ route('transaction.addCustomProduct')}}">
                @csrf
                <input type="hidden" name="idsales" value="{{ $idsales }}">
                <div class="modal-header">
                    <h3 class="modal-title"> Add Custom Product </h3>
                </div>

                <div class="modal-body" id="modal-return-body">
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="chargeType">Charge Type:</label>
                        <div class="col-sm-8 col-md-8">
                            <select id="chargeType" name="chargeType" class="form-control">
                                <option value="live">Live Product</option>
                                <option value="service">Service Charge</option>
                                <option value="delivery">Delivery Charge</option>
                                <option value="other">Other Charges</option>
                            </select> 
                        </div>
                    </div><br>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="product_name">Product Name:</label>
                        <div class="col-sm-8 col-md-8">
                            <input type="text" id="product_name" style="text-transform:uppercase" name="product_name" class="form-control">
                        </div>
                    </div><br>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="product_price">Price:</label>
                        <div class="col-sm-8 col-md-8">
                            <input type="number" id="product_price" name="product_price" class="form-control">
                        </div>
                    </div><br>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="product_quantity">Quantity:</label>
                        <div class="col-sm-8 col-md-8">
                            <input type="number" id="product_quantity" name="product_quantity" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>