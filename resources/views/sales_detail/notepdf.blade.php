@extends('layouts.app')

@section('title')
   Print Receipt
@endsection

@section('content')     

<table width="100%">
<tr>
   <td rowspan="3" width="60%"><img src="{{ URL::to('/') }}/images/{{$setting->logo}}" width="150"><br>
   {{ $setting->address }}<br><br>
   </td>
   <td>Date</td>
   <td>: {{ date('Y-m-d') }}</td>
</tr>     
<tr>
   <td>Order No.</td>
   <td><h2>{{ $sales->sales_id }}</h2></td>
</tr>
</table>
         
<table width="100%" class="data">
<thead>
   <tr>
      <th>No</th>
      <th>Product Code</th>
      <th>Product Name</th>
      <th>Unit price</th>
      <th>Total Quantity</th>
      <th>Discount</th>
      <th>Subtotal</th>
   </tr>

   <tbody>
      @foreach($detail as $data)
         <tr>
            <td>{{ ++$no }}</td>
            <td>{{ $data->product_code }}</td>
            <td>{{ $data->product_name }}</td>
            <td align="right">{{ format_money($data->price) }}</td>
            <td>{{ $data->total }}</td>
            <td align="right">{{ format_money($data->discount) }}</td>
            <td align="right">{{ format_money($data->sub_total) }}</td>
         </tr>
      @endforeach
   
   </tbody>
   <tfoot>
      <tr><td colspan="6" align="right"><b>Total Price</b></td><td align="right"><b>{{ format_money($sales->total_price) }}</b></td></tr>
      <tr><td colspan="6" align="right"><b>Total Amount</b></td><td align="right"><b>{{ format_money($sales->pay) }}</b></td></tr>
      <tr><td colspan="6" align="right"><b>Cash Received</b></td><td align="right"><b>{{ format_money($sales->received) }}</b></td></tr>
      <tr><td colspan="6" align="right"><b>Change</b></td><td align="right"><b>{{ format_money($sales->received - $sales->pay) }}</b></td></tr>
   </tfoot>
</table>

<table width="100%">
   <tr>
      <td>
         <b>Thank you for shopping and have a nice day!</b>
      </td>
      <td align="right">
         Cashier: &nbsp;{{Auth::user()->name}}
      </td>
   </tr>
</table>
@endsection

@section('script')
   <style type="text/css">
      table td{font: arial 12px;}
      table.data td,
      table.data th{
         border: 1px solid #ccc;
         padding: 5px;
      }
      table.data th{
         text-align: center;
      }
      table.data{ border-collapse: collapse }
   </style>

   <script type="text/javascript">

      // var afterPrint = function () 
      // {
      //    window.location.href = "{{ route('home')}}";
      // };

      // if (window.matchMedia) 
      // {
      //    var mediaQueryList = window.matchMedia('print');
      //    mediaQueryList.addListener(function (mql) {
      //          // alert($(mediaQueryList).html());
      //          if (mql.matches) {
      //             // beforePrint();
      //          } else {
      //             afterPrint();
      //          }
      //    });
      // }

      // window.onafterprint = afterPrint;

      $(document).ready(function() 
      {
         setTimeout(function() { 
            window.location.href = "{{ route('home')}}";
         }, 10000);
         //  window.print()
      });
   </script>
@endsection