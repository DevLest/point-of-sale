<div class="modal" id="modal-returnID" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-m">
        <div class="modal-content" style="text-align:center;">

            <div class="modal-header">
                <h3 class="modal-title">
                    Please enter Order ID</h3>
            </div>

            <div class="modal-body">

                <div class="alert alert-danger" id="returnID-alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>ERROR! </strong> <div id="returnID-message"></div>
                </div>
                <input type="text" style="text-align:center;" class="form-control" name="returnID" id="returnID">
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" onclick="seachReturnID()">Submit</button>
            </div>

        </div>
    </div>
</div>

<div class="modal" id="modal-return" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-xl">
        <div class="modal-content" style="text-align:center;">
            <form method="POST" id="modal-return-form" action="{{ route('transaction.returnSubmit')}}">
                @csrf
                <div class="modal-header">
                    <h3 class="modal-title"> Confirm Return </h3>
                </div>

                <div class="modal-body" id="modal-return-body">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>