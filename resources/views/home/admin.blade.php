@extends('layouts.app')

@section('title')
    Dashboard
@endsection

@section('breadcrumb')
    @parent
    <li>Dashboard</li>
@endsection

@section('head-style')
    @parent
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{ format_money($today_sales) }}</h3>
                    <p class="label label-success">Net Profit : {{ format_money($today_sales_net) }}</p>
                    <p class="text=info">Today Sales Total</p>
                </div>
                <div class="icon">
                    <i class="fa fa-truck"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{ format_money($month_sales) }}</h3>
                    <p class="label label-success">Net Profit :{{ format_money($month_sales_net) }}</p>
                    <p class="text=info">Current Month Sales</p>
                </div>
                <div class="icon">
                    <i class="fa fa-archive"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>{{ format_money($last_month_sales) }}</h3>
                    <p class="label label-success">Net Profit :{{ format_money($last_month_sales_net) }}</p>
                    <p class="text=info">Last Monthly Sales</p>
                </div>
                <div class="icon">
                    <i class="fa fa-credit-card"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{ format_money($annual_sales) }}</h3>
                    <p class="label label-success">Net Profit :{{ format_money($annual_sale_net) }}</p>
                    <p class="text=info">Annual Sales Total</p>
                </div>
                <div class="icon">
                    <i class="fa fa-cubes"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="box">
                <div class="box-header with-border">
                    Today's Top 10 Sales per Product
                </div>
                <div class="box-body">
                    {!! $salesChart->render() !!}
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box">
                <div class="box-header with-border">
                    Today's Top 10 Sold Transactions
                </div>
                <div class="box-body">
                    {!! $quantityChart->render() !!}
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box">
                <div class="box-header with-border">
                    Top Categories of the Month
                </div>
                <div class="box-body">
                    {!! $categoryChart->render() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    {!! $weeklyChart->render() !!}
                </div>
            </div>
            <div class="row">
                <div class="box">
                    <div class="box-header with-border">
                        Top Products of the Month
                    </div>
                    <div class="box-body">
                        {!! $productChart->render() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    Critical Inventory | Low stock items
                </div>
                <div class="box-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Product Code</th>
                                <th>Name</th>
                                <th>Remaining Inventory</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        var table;
        $(function(){
            table = $('.table').DataTable({
                "processing" : true,
                "serverside" : true,
                "ajax" : {
                "url" : "{{ route('home.inventory') }}",
                "type" : "GET"
                },
                'columnDefs': [{
                    'targets': 0,
                    'searchable': false,
                    'orderable': false
                }],
                'order': [1, 'asc'],
                "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $('td', nRow).css('color', 'Red');
                }
            }); 
        })
    </script>
@endsection
