@extends('layouts.app')

@section('title')
Cashier
@endsection

@section('breadcrumb')
@parent
<li>Dashboard</li>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body text-center">
                <h2>Welcome</h2>
                <br><br>
                <div id="newtrans" class="btn btn-success btn-lg" onclick="redirect()">New Transaction</div>
                <br><br><br>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-xs-12">
        <div class="small-box bg-blue">
            <div class="inner text-center">
                <h1>{{ format_money($today_sales) }}</h1>
                <p class="text=info">Today Sales Total</p>
            </div>
            <div class="icon">
                <i class="fa fa-truck"></i>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">

    $(document).on('keypress', function (e) {
        if (e.which == 13) {
            $(location).prop('href', "{{ route('transaction.new') }}")
        }
    });

    function redirect()
    {
        window.location.href = "{{ route('transaction.new') }}";
    }
</script>
@endsection
