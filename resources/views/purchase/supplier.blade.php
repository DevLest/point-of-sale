<div class="modal" id="modal-supplier" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
     
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> &times; </span> </button>
      <h3 class="modal-title">Select Supplier</h3>
   </div>
            
<div class="modal-body">
   <table class="table table-striped tabel-supplier">
      <thead>
         <tr>
            <th>Supplier Name</th>
            <th>Address</th>
            <th>Phone</th>
            <th>Action</th>
         </tr>
      </thead>
      <tbody>
         @foreach($supplier as $data)
         <tr>
            <th>{{ $data->supplier_name }}</th>
            <th>{{ $data->address }}</th>
            <th>{{ $data->phone }}</th>
            <th><a href="purchase/{{ $data->supplier_id }}/add" class="btn btn-primary"><i class="fa fa-check-circle"></i> Add</a></th>
          </tr>
         @endforeach
      </tbody>
   </table>

</div>
      
         </div>
      </div>
   </div>
