@extends('layouts.app')

@section('title')
  Purchase List
@endsection

@section('breadcrumb')
   @parent
   <li>purchase</li>
@endsection

@section('content')     
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> New Transaction</a>
        @if(!empty(session('idpurchase')))
        <a href="{{ route('sales.index') }}" class="btn btn-info"><i class="fa fa-plus-circle"></i> Active Transaction</a>
        @endif
      </div>
      <div class="box-body">  

<table class="table table-striped tabel-purchase">
<thead>
   <tr>
      <th width="30">No</th>
      <th>Date</th>
      <th>Supplier</th>
      <th>Total Items</th>
      <th>Total Amount</th>
      <th>Discount</th>
      <th>Total Payment</th>
      <th width="100">Action</th>
   </tr>
</thead>
<tbody></tbody>
</table>

      </div>
    </div>
  </div>
</div>

@include('purchase.detail')
@include('purchase.supplier')
@endsection

@section('script')
<script type="text/javascript">
var table, save_method, table1;
$(function(){
   table = $('.tabel-purchase').DataTable({
     "processing" : true,
     "serverside" : true,
     "ajax" : {
       "url" : "{{ route('purchase.data') }}",
       "type" : "GET"
     }
   }); 
   
   table1 = $('.tabel-detail').DataTable({
     "dom" : 'Brt',
     "bSort" : false,
     "processing" : true
    });

   $('.tabel-supplier').DataTable();
});

function addForm(){
   $('#modal-supplier').modal('show');        
}

function showDetail(id){
    $('#modal-detail').modal('show');

    table1.ajax.url("purchase/"+id+"/lihat");
    table1.ajax.reload();
}

function deleteData(id){
   if(confirm("Are you sure the data will be deleted?")){
     $.ajax({
       url : "purchase/"+id,
       type : "POST",
       data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
       success : function(data){
         table.ajax.reload();
       },
       error : function(){
         alert("Unable to delete data!");
       }
     });
   }
}
</script>
@endsection