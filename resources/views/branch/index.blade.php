@extends('layouts.app')

@section('title')
  Branch Management
@endsection

@section('breadcrumb')
   @parent
   <li>branch</li>
@endsection

@section('content') 
    <style>
    .file {
        position: relative;
        overflow: hidden;
    }
    .file input {
        position: absolute;
        font-size: 50px;
        opacity: 0;
        right: 0;
        top: 0;
    }
    </style>    
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                </div>
                <div class="box-body">  
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Branch Name</th>
                                <th>Branch Location</th>
                                <th>Branch Code</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script type="text/javascript">
    var table, save_method;
    $(function(){
        table = $('.table').DataTable({
            "processing" : true,
            "serverside" : true,
            "ajax" : {
            "url" : "{{ route('branch.data') }}",
            "type" : "GET"
            },
            'columnDefs': [{
                'targets': 0,
                'searchable': false,
                'orderable': false
            }],
            'order': [1, 'asc']
        }); 
    });
</script>
@endsection