@extends('layouts.app')

@section('title')
    Transfer Inventory to {{ App\Branch::find($transfer->to_branch_id)->name }}
@endsection

@section('head-style')
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 30px;
            height: 20px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 12px;
            width: 12px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked+.slider {
            background-color: #2196F3;
        }

        input:focus+.slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked+.slider:before {
            -webkit-transform: translateX(12px);
            -ms-transform: translateX(12px);
            transform: translateX(12px);
        }

        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }

        .file {
            position: relative;
            overflow: hidden;
        }

        .file input {
            position: absolute;
            font-size: 50px;
            opacity: 0;
            right: 0;
            top: 0;
        }

    </style>
@endsection

@section('breadcrumb')
    @parent
    <li>transfer</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                </div>
                <div class="box-body">
                    <form class="form form-horizontal form-product" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="transferIdvalue" id="transferIdvalue" value="{{$transfer_id}}">
                        <div class="form-group">

                            <div class="alert alert-danger" id="success-alert">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong>ERROR! </strong> Product code not found or Inventory is zero
                            </div>

                            <div class="alert alert-warning" id="continuation-alert">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong>Warning! </strong> There is already a pre-saved data from previous transfer. <br> Clear if data not needed.
                            </div>

                            <label for="code" class="col-md-2 control-label">Product Code</label>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <input id="code" type="text" class="form-control" name="code" autofocus required>
                                    <span class="input-group-btn">
                                        <button onclick="showProduct()" type="button" class="btn btn-info">...</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </form>

                    <form class="form-cart">
                        {{ csrf_field() }} {{ method_field('PATCH') }}
                        <table class="table table-striped table-sales table-responsive">
                            <thead>
                                <tr>
                                    <th>Product Code</th>
                                    <th>Product Name</th>
                                    <th align="right">Price</th>
                                    <th>Available Quantity</th>
                                    <th>Total Quantity</th>
                                    <th width="100">Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </form>
                </div>
                <div class="box-footer text-right">
                    <a onclick="clearForm()" class="btn btn-danger"><i class="fa fa-eraser"></i> Clear Data</a>
                    <a onclick="saveForm()" class="btn btn-success"><i class="fa fa-save"></i> Submit Transfer</a>
                </div>
            </div>
        </div>
    </div>
    @include('transfer.toBranch')
    @include('sales_detail.product')
@endsection

@section('script')
    <script type="text/javascript">
        $(document).on('keypress', function(e) {
            if (e.which == 61) {
                showProduct();
            }
        });

        var table;
        $(function() {
            table = $('.table-sales').DataTable({
                "dom": 'Brt',
                "bSort": false,
                "pageLength": 100,
                "processing": true,
                "ajax": {
                    "url": "{{ route('transfer.newData') }}",
                    "type": "GET"
                }
            }).on('draw.dt', function() {
            });

            $('.table-product').DataTable();
            $("#success-alert").hide();
            
            if ("{{ $continueMsg }}" == 1)
            {
                $("#continuation-alert").fadeTo(6000, 500).slideUp(500, function() {
                    $("#continuation-alert").slideUp(500);
                });
            }
            else $("#continuation-alert").hide();

            $('.form-product').on('submit', function() {
                return false;
            });

            $('#code').change(function() {
                addItem();
            });

            $('.form-cart').submit(function() {
                return false;
            });

            $('.save').click(function() {
                $('.form-sales').submit();
            });
        });

        function addItem() {
            $.ajax({
                url: "{{ route('transfer.store') }}",
                type: "POST",
                data: $('.form-product').serialize(),
                success: function(data) {
                    if ($.trim(data) == "Good") {
                        $('#code').val('').focus();
                        table.ajax.reload(function() {
                        });
                    } else {
                        $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                            $("#success-alert").slideUp(500);
                        });
                    }
                },
                error: function(e) {
                    alert("Unable to save data!");
                }
            });
        }

        function showProduct() {
            $('#modal-product').modal('show');
        }

        function selectItem(code) {
            $('#code').val(code);
            $('#modal-product').modal('hide');
            addItem();
        }

        function changeCount(id) {
            $.ajax({
                url: "{{ route('transfer.index').'/' }}" + id,
                type: "POST",
                data: $('.form-cart').serialize(),
                success: function(data) {
                    $('#code').focus();
                    table.ajax.reload(function() {
                    });
                },
                error: function(e) {
                    console.log(e.responseText);
                    alert("Unable to save data!");
                }
            });
        }

        function deleteItem(id) {
            if (confirm("Are sure the data will be deleted?")) {
                $.ajax({
                    url: "{{ route('transfer.index').'/' }}" + id,
                    type: "POST",
                    data: {
                        '_method': 'DELETE',
                        '_token': $('meta[name=csrf-token]').attr('content')
                    },
                    success: function(data) {
                        table.ajax.reload(function() {
                        });
                    },
                    error: function() {
                        alert("Cannot delete data!");
                    }
                });
            }
        }

        function formclose() {
            window.location.replace("{{ route('transfer.index') }}")
        }

        function receive() {
            $.ajax({
                url: "{{ route('transfer.new') }}",
                type: "POST",
                data: $('.form-tobranch').serialize(),
                success: function(data) {
                    if (data != "Bad")
                    {
                        $('#transferIdvalue').val(data);
                        $("#modal-transferID").hide();
                        loadAll();
                    }
                },
                error: function(e) {
                    console.log(e.responseText);
                }
            });
        }

        function clearForm()
        {
            if (confirm("Are sure? The data will be deleted?")) {
                $.ajax({
                    url: "{{ route('transfer.clearSession') }}",
                    type: "POST",
                    data: $('.form-tobranch').serialize(),
                    success: function(data) {
                        var form = $('<form action="' + "{{ route('transfer.new') }}" + '" method="post">@csrf<input type="text" name="target" value="{{$transfer->to_branch_id}}" /></form>');
                        $('body').append(form);
                        form.submit();
                    },
                    error: function(e) {
                        console.log(e.responseText);
                    }
                });
            }
        }

        function saveForm()
        {
            if (confirm("Are sure you want to save Data? This method is irreversible?")) {
                $.ajax({
                    url: "{{ route('transfer.index')}}"+"/"+"{{$transfer_id}}"+"/edit",
                    type: "GET",
                    success: function(data) {
                        window.location.replace("{{ route('transfer.pdf') }}")
                    },
                    error: function(e) {
                        console.log(e.responseText);
                    }
                });
            }
        }
    </script>
@endsection
