@extends('layouts.app')

@section('title')
   Print Receipt Transfer
@endsection

@section('content')     

<table width="100%">
    <tr>
    <td rowspan="3" width="60%"><img src="{{ URL::to('/') }}/images/{{$setting->logo}}" width="150"><br>
            {{ $setting->address }}<br><br>
        </td>
        <td>Date</td>
        <td>: {{ date('Y-m-d') }}</td>
    </tr>     
    <tr>
        <td>Branch Target</td>
        <td>:{{ $receiving->name }}</td>
    </tr>   
    <tr>
        <td>Transfer No.</td>
        <td><h1>{{ $receiving->id }}</h1></td>
    </tr>
</table>
         
<table width="100%" class="data">
    <thead>
        <tr>
            <th>Product Code</th>
            <th>Product Name</th>
            <th>Total Quantity</th>
        </tr>
    </thead>

    <tbody>
        @foreach($details as $data)
            <tr>
                <td>{{ $data->product_code }}</td>
                <td>{{ $data->product_name }}</td>
                <td align="center" >{{ $data->quantity }}</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
    </tfoot>
</table>

<br><br>
<table width="100%">
   <tr>
      <td>
         <b>Please submit this to branch manager for receiving of items to target destination.</b>
      </td>
      <td align="right">
         User: &nbsp;{{Auth::user()->name}}
      </td>
   </tr>
</table>
@endsection

@section('script')
   <style type="text/css">
      table td{font: arial 12px;}
      table.data td,
      table.data th{
         border: 1px solid #ccc;
         padding: 5px;
      }
      table.data th{
         text-align: center;
      }
      table.data{ border-collapse: collapse }
   </style>

   <script type="text/javascript">

      var afterPrint = function () 
      {
         window.location.href = "{{ route('transfer.index')}}";
      };

      if (window.matchMedia) 
      {
         var mediaQueryList = window.matchMedia('print');
         mediaQueryList.addListener(function (mql) 
         {
               if (mql.matches) {
                  // beforePrint();
               } else {
                  afterPrint();
               }
         });
      }

      window.onafterprint = afterPrint;

      $(document).ready(function() 
      {
          window.print()
      });
   </script>
@endsection