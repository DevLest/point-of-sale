<div class="modal" id="modal-tobranch" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <form class="form form-horizontal form-tobranch" data-toggle="validator" method="post" action="{{ route('transfer.new') }}">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> &times; </span> </button>
                    <h5 class="modal-title">Choose Target Branch</h5>
                </div>
                <div class="modal-body">
                    
                    <div class="form-group">
                        <label for="target" class="col-md-3 control-label">Target Branch</label>
                        <div class="col-md-9">
                            <select class="form-control" id="target" name="target" aria-label="Please choose origin Branch" required>
                                @foreach ( App\Branch::where('branch_code','!=',env('BRANCH_CODE'))->get() as $branch )
                                    <option value="{{$branch->id}}">{{$branch->name}}
                                        <small> | {{$branch->location}}</small>
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </div>
        </form>
    </div>
</div>
