@extends('layouts.app')

@section('title')
  Inventory Transfer
@endsection

@section('breadcrumb')
   @parent
   <li>transfer</li>
@endsection

@section('content') 
    <style>
    .file {
        position: relative;
        overflow: hidden;
    }
    .file input {
        position: absolute;
        font-size: 50px;
        opacity: 0;
        right: 0;
        top: 0;
    }
    </style>    
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a data-toggle="modal" data-target="#modal-tobranch" class="btn btn-success"><i class="fa fa-plus-circle"></i> Transfer Inventory</a>
                </div>
                <div class="box-body">  
                    <form method="post" id="form-product">
                        {{ csrf_field() }}
                        <table class="table table-striped table-main">
                            <thead>
                                <tr>
                                    <th>Transfer ID</th>
                                    <th>SKU Count / Products Count</th>
                                    <th>Total Transfered Quantity</th>
                                    <th>Target Branch</th>
                                    <th>Transfer Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </form>

                </div>
            </div>
        </div>
    </div>
    @include('transfer.toBranch')
    @include('transfer.detail')
@endsection

@section('script')
<script type="text/javascript">
    var table, save_method, table1;
    $(function(){
        table = $('.table-main').DataTable({
            "processing" : true,
            "serverside" : true,
            "ajax" : {
            "url" : "{{ route('transfer.data') }}",
            "type" : "GET"
            },
            'columnDefs': [{
                'targets': 0,
                'searchable': false,
                'orderable': false
            }],
            'order': [1, 'asc']
        }); 

        $('#fileupload').on("change", function(){ $('#fileUp').submit(); });
        
        $('#select-all').click(function(){
            $('input[type="checkbox"]').prop('checked', this.checked);
        });
        
        table1 = $('.tabel-detail').DataTable({
            "dom": 'Brt',
            "bSort": false,
            "processing": true
        });

        $('#modal-form form').validator().on('submit', function(e){
            if(!e.isDefaultPrevented())
            {
                var id = $('#id').val();
                if(save_method == "add") url = "{{ route('product.store') }}";
                else url = "product/"+id;
                
                $.ajax({
                url : url,
                type : "POST",
                data : $('#modal-form form').serialize(),
                dataType: 'JSON',
                success : function(data){
                    if(data.msg=="error"){
                        alert('The product code has been used!');
                        $('#code').focus().select();
                    }else{
                        $('#modal-form').modal('hide');
                        table.ajax.reload();
                    }            
                },
                error : function(e){
                    alert("Unable to save data!");
                }   
                });
                return false;
            }
        });
    });
    
    function showDetail(id) {
        $('#modal-detail').modal('show');

        table1.ajax.url("transfer/" + id);
        table1.ajax.reload();
    }

    function addForm(){
        save_method = "add";
        $('input[name=_method]').val('POST');
        $('#modal-form').modal('show');
        $('#modal-form form')[0].reset();            
        $('.modal-title').text('Add Product');
        $('#kode').attr('readonly', false);
    }

    function editForm(id){
        save_method = "edit";
        $('input[name=_method]').val('PATCH');
        $('#modal-form form')[0].reset();
        $.ajax({
            url : "product/"+id+"/edit",
            type : "GET",
            dataType : "JSON",
            success : function(data){
            $('#modal-form').modal('show');
            $('.modal-title').text('Edit product');
            
            $('#id').val(data.product_id);
            $('#code').val(data.product_code).attr('readonly', true);
            $('#name').val(data.product_name);
            $('#category').val(data.category_id);
            $('#brand').val(data.brand);
            $('#cost').val(data.cost);
            $('#price').val(data.price);
            $('#wholesale_price').val(data.wholesale_price);
            $('#stock').val(data.stock);
            $('#stock_min').val(data.stock_min);
            
            },
            error : function($e){
            alert("Cannot display data!");
            }
        });
    }

    function deleteData(id){
        if(confirm("Are sure the data will be deleted?")){
            $.ajax({
                url : "product/"+id,
                type : "POST",
                data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                success : function(data){
                    table.ajax.reload();
                },
                error : function(){
                    alert("Cannot delete data!");
                }
            });
        }
    }

    function deleteAll(){
        if($('input:checked').length < 1){
            alert('Select the data to be deleted!');
        }else if(confirm("Are sure to delete all selected data?")){
            $.ajax({
                url : "product/delete",
                type : "POST",
                data : $('#form-product').serialize(),
                success : function(data){
                    table.ajax.reload();
                },
                error : function(){
                    alert("Cannot delete data!");
                }
            });
        }
    }
</script>
@endsection