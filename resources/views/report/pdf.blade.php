<!DOCTYPE html>
<html>
<head>  
  <title>Product PDF</title>
  <link rel="stylesheet" href="{{ asset('public/adminLTE/bootstrap/css/bootstrap.min.css') }}">
</head>
<body>
 
<h3 class="text-center">Income Report</h3>
<h4 class="text-center">Income Report as of {{ date("F j, Y, g:i", strtotime($start)) }} to {{ date("F j, Y, g:i", strtotime($end)) }} </h4>

         
<table class="table table-striped">
<thead>
   <tr>
    <th>No</th>
    <th>Date</th>
    <th>Sales</th>
    <th>Purchase</th>
    <th>Expenditure</th>
    <th>Income</th>
   </tr>

   <tbody>
    @foreach($data as $row)    
    <tr>
    @foreach($row as $col)
     <td>{{ $col }}</td>
    @endforeach
    </tr>
    @endforeach
   </tbody>
</table>

</body>
</html>