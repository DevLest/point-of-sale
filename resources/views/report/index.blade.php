@extends('layouts.app')

@section('title')
  Income Report as of {{ date("F j, Y, g:i", strtotime($start)) }} to {{ date("F j, Y, g:i", strtotime($end)) }}
@endsection

@section('breadcrumb')
   @parent
   <li>Report</li>
@endsection

@section('content')     
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <a onclick="periodeForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Change Period</a>
        <a href="reporting/pdf/{{$start}}/{{$end}}" target="_blank" class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Export PDF</a>
      </div>
      <div class="box-body">  

<table class="table table-striped tabel-report">
<thead>
   <tr>
      <th width="30">No</th>
      <th>Date</th>
      <th>Sales</th>
      <th>Purchase</th>
      <th>Expenditure</th>
      <th>Income</th>
   </tr>
</thead>
<tbody></tbody>
</table>

      </div>
    </div>
  </div>
</div>

@include('report.form')
@endsection

@section('script')
<script type="text/javascript">
var table, start, end;
$(function(){
   $('#start, #end').datepicker({
     format: 'yyyy-mm-dd',
     autoclose: true
   });

   table = $('.tabel-report').DataTable({
     "dom" : 'Brt',
     "bSort" : false,
     "bPaginate" : false,
     "processing" : true,
     "serverside" : true,
     "ajax" : {
       "url" : "reporting/data/{{ $start }}/{{ $end }}",
       "type" : "GET"
     }
   }); 

});

function periodeForm(){
   $('#modal-form').modal('show');        
}

</script>
@endsection