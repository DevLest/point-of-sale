<!DOCTYPE html>
<html>
<head>
   <title>Print Barcode</title>
</head>
<body>
   <table width="100%">   
     <tr>
      
      @foreach($dataproduct as $data)
      <td align="center" style="border: 1px solid #ccc">
      {{ $data->product_name}} - PhP {{ format_money($data->price) }}</span><br><br>
      <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG( $data->product_code, 'C39') }}" height="60" width="180">
      <br>{{ $data->product_code}}
      </td>
      @if( $no++ % 3 == 0)
         </tr><tr>
      @endif
     @endforeach
     
     </tr>
   </table>
</body>
</html>