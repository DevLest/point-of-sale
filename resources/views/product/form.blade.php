<div class="modal" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog modal-lg">
     <div class="modal-content">
   
  <form class="form-horizontal" data-toggle="validator" method="post">
  {{ csrf_field() }} {{ method_field('POST') }}
  
  <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> &times; </span> </button>
     <h3 class="modal-title"></h3>
  </div>
       
<div class="modal-body">
 
 <input type="hidden" id="id" name="id">
 <div class="form-group">
   <label for="code" class="col-md-3 control-label">Product Code</label>
   <div class="col-md-6">
     <input id="code" type="text" class="form-control" name="code" autofocus required>
     <span class="help-block with-errors"></span>
   </div>
 </div>

 <div class="form-group">
   <label for="name" class="col-md-3 control-label">Name</label>
   <div class="col-md-6">
     <input id="name" type="text" class="form-control" name="name" required>
     <span class="help-block with-errors"></span>
   </div>
 </div>

 <div class="form-group">
   <label for="category" class="col-md-3 control-label">Category</label>
   <div class="col-md-6">
     <select id="category" type="text" class="form-control" name="category" required>
       <option value=""> -- Choose Category-- </option>
       @foreach($category as $list)
         <option value="{{ $list->category_id }}">{{ $list->category }}</option>
       @endforeach
     </select>
     <span class="help-block with-errors"></span>
   </div>
 </div>

 <div class="form-group">
   <label for="brand" class="col-md-3 control-label">Brand</label>
   <div class="col-md-6">
     <input id="brand" type="text" class="form-control" name="brand" value="Generic">
     <span class="help-block with-errors"></span>
   </div>
 </div>

 <div class="form-group">
   <label for="cost" class="col-md-3 control-label">Cost</label>
   <div class="col-md-3">
     <input id="cost" type="number" class="form-control" name="cost" required>
     <span class="help-block with-errors"></span>
   </div>
 </div>

 <div class="form-group">
   <label for="wholesaleprice" class="col-md-3 control-label">Whole Sale Price</label>
   <div class="col-md-2">
     <input id="wholesaleprice" type="text" class="form-control" name="wholesaleprice" value="0">
     <span class="help-block with-errors"></span>
   </div>
 </div>

 <div class="form-group">
   <label for="price" class="col-md-3 control-label">Price</label>
   <div class="col-md-3">
     <input id="price" type="number" class="form-control" name="price" required>
     <span class="help-block with-errors"></span>
   </div>
 </div>

 <div class="form-group">
   <label for="stock" class="col-md-3 control-label">Stock</label>
   <div class="col-md-2">
     <input id="stock" type="number" class="form-control" name="stock" required>
     <span class="help-block with-errors"></span>
   </div>
 </div>

 <div class="form-group">
   <label for="stock_limit" class="col-md-3 control-label">Stock Limit</label>
   <div class="col-md-2">
     <input id="stock_limit" type="number" class="form-control" name="stock_limit" required>
     <span class="help-block with-errors"></span>
   </div>
 </div>
 
</div>
  
  <div class="modal-footer">
     <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i>Save </button>
     <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
  </div>
   
  </form>

        </div>
     </div>
  </div>