@extends('layouts.app')

@section('title')
Product List
@endsection

@section('breadcrumb')
@parent
<li>product</li>
@endsection

@section('content')
<style>
    .file {
        position: relative;
        overflow: hidden;
    }

    .file input {
        position: absolute;
        font-size: 50px;
        opacity: 0;
        right: 0;
        top: 0;
    }

</style>
<div class="row">
    <div class="col-xs-12">
        <div class="box">

            @if(isset($error))
                <div class="alert alert-warning" id="success-alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    @if(floatval($error) >= 0 )
                        <strong>Uplead complete! </strong> A total of {{ $success }} product SKU's have been uploaded
                        and a total of {{ $error }} SKU's were unsuccessful
                    @else
                        <strong>Error! </strong> {{ $error }}
                    @endif
                </div>
            @endif

            <div class="box-header">
                <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add</a>
                @if($user->level == 0)
                    <a onclick="deleteAll()" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>
                @endif
                <a onclick="printBarcode()" class="btn btn-info"><i class="fa fa-barcode"></i> Print Barcode</a>
                <a onclick="exportProducts()" class="btn btn-primary"><i class="fa fa-sign-out"></i> Export Products</a>
                <a onclick="exportInventory()" class="btn btn-normal"><i class="fa fa-sign-out"></i> Export Inventory Balance</a>

                <div class="file btn btn-lg btn-warning" style="font-size: 15px;">
                    <i class="fa fa-upload"></i>
                    Upload
                    <form class="form-horizontal" method="post" enctype="multipart/form-data" id="fileUp"
                        action="{{ route('product.upload') }}">
                        {{ csrf_field() }} {{ method_field('POST') }}
                        <input type="file" name="fileupload" id="fileupload" multiple />
                    </form>
                </div>
            </div>
            <div class="box-body">
                <form method="post" id="form-product">
                    {{ csrf_field() }}
                    <table class="table table-striped" id="table-product">
                        <thead>
                            <tr>
                                <th width="20"><input type="checkbox" value="1" id="select-all"></th>
                                <th width="20">No</th>
                                <th>Product Code</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Brand</th>
                                <th>Cost</th>
                                <th>Price</th>
                                <th>Whole Sale</th>
                                <th>Stock</th>
                                <th>Stock Limit</th>
                                <th width="100">Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </form>
            </div>
            <br>
            <div class="row">
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3>Recently Received Products</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-striped" id="table-product-received">
                                <thead>
                                    <tr>
                                        <th>Product Code</th>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Brand</th>
                                        <th>Origin</th>
                                        <th>Quantity</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3>Recently Transfered Products</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-striped" id="table-product-transfered">
                                <thead>
                                    <tr>
                                        <th>Product Code</th>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Brand</th>
                                        <th>Destination</th>
                                        <th>Quantity</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@include('product.form')
@endsection

@section('script')
<script type="text/javascript">
    var table, save_method;
    $(function () {
        table = $('#table-product').DataTable({
            "processing": true,
            "serverside": true,
            "ajax": {
                "url": "{{ route('product.data') }}",
                "type": "GET"
            },
            'columnDefs': [{
                'targets': 0,
                'searchable': false,
                'orderable': false
            }],
            'order': [1, 'asc']
        });

        tableReceived = $('#table-product-received').DataTable({
            "processing": true,
            "serverside": true,
            "ajax": {
                "url": "{{ route('product.data.received') }}",
                "type": "GET"
            },
            'columnDefs': [{
                'targets': 0,
                'searchable': false,
                'orderable': false
            }],
            'order': [1, 'asc']
        });

        tableTransfered = $('#table-product-transfered').DataTable({
            "processing": true,
            "serverside": true,
            "ajax": {
                "url": "{{ route('product.data.transfered') }}",
                "type": "GET"
            },
            'columnDefs': [{
                'targets': 0,
                'searchable': false,
                'orderable': false
            }],
            'order': [1, 'asc']
        });

        $('#fileupload').on("change", function () {
            $('#fileUp').submit();
        });

        $('#select-all').click(function () {
            $('input[type="checkbox"]').prop('checked', this.checked);
        });

        $('#modal-form form').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                var id = $('#id').val();
                if (save_method == "add") url = "{{ route('product.store') }}";
                else url = "product/" + id;

                $.ajax({
                    url: url,
                    type: "POST",
                    data: $('#modal-form form').serialize(),
                    dataType: 'JSON',
                    success: function (data) {
                        if (data.msg == "error") {
                            alert('The product code has been used!');
                            $('#code').focus().select();
                        } else {
                            $('#modal-form').modal('hide');
                            table.ajax.reload();
                        }
                    },
                    error: function (xhr, status, error) {
                        alert("Unable to save data!");
                    }
                });
                return false;
            }
        });
    });

    function addForm() {
        save_method = "add";
        $('input[name=_method]').val('POST');
        $('#modal-form').modal('show');
        $('#modal-form form')[0].reset();
        $('.modal-title').text('Add Product');
        $('#code').attr('readonly', false);
    }

    function editForm(id) {
        save_method = "edit";
        $('input[name=_method]').val('PATCH');
        $('#modal-form form')[0].reset();
        $.ajax({
            url: "product/" + id + "/edit",
            type: "GET",
            dataType: "JSON",
            success: function (data) {

                prod = data.data;
                user = data.user;
                $('#modal-form').modal('show');
                $('.modal-title').text('Edit product');

                $('#id').val(prod.product_id);
                $('#code').val(prod.product_code).attr('readonly', true);
                $('#name').val(prod.product_name);
                $('#category').val(prod.category_id);
                $('#brand').val(prod.brand);
                $('#cost').val(prod.cost);
                $('#wholesaleprice').val(prod.wholesaleprice);
                $('#price').val(prod.price);
                $('#stock').val(prod.stock);
                $('#stock_limit').val(prod.stock_limit);

                if (user.level !== 0) {
                    $("#price").prop("disabled", true);
                    $("#wholesaleprice").prop("disabled", true);
                    $("#cost").prop("disabled", true);
                    $("#stock").prop("disabled", true);
                }

            },
            error: function () {
                alert("Cannot display data!");
            }
        });
    }

    function deleteData(id) {
        if (confirm("Are sure the data will be deleted?")) {
            $.ajax({
                url: "product/" + id,
                type: "POST",
                data: {
                    '_method': 'DELETE',
                    '_token': $('meta[name=csrf-token]').attr('content')
                },
                success: function (data) {
                    table.ajax.reload();
                },
                error: function () {
                    alert("Cannot delete data!");
                }
            });
        }
    }

    function deleteAll() {
        if ($('input:checked').length < 1) {
            alert('Select the data to be deleted!');
        } else if (confirm("Are sure to delete all selected data?")) {
            $.ajax({
                url: "product/delete",
                type: "POST",
                data: $('#form-product').serialize(),
                success: function (data) {
                    table.ajax.reload();
                },
                error: function () {
                    alert("Cannot delete data!");
                }
            });
        }
    }

    function printBarcode() {
        if ($('input:checked').length < 1) {
            alert('Select the data to be printed!');
        } else {
            $('#form-product').attr('target', '_blank').attr('action', "product/print").submit();
        }
    }

    function exportProducts() {
        if ($('input:checked').length < 1) {
            alert('Select the data to be exported!');
        } else if (confirm("Are sure to export all selected data?")) {
            $.ajax({
                url: "product/export",
                type: "POST",
                data: $('#form-product').serialize(),
                success: function (data) {
                    var downloadLink = document.createElement("a");
                    var fileData = ['\ufeff'+data];

                    var blobObject = new Blob(fileData,{
                        type: "text/csv;charset=utf-8;"
                    });

                    var url = URL.createObjectURL(blobObject);
                    downloadLink.href = url;
                    downloadLink.download = "products.csv";

                    /*
                    * Actually download CSV
                    */
                    document.body.appendChild(downloadLink);
                    downloadLink.click();
                    document.body.removeChild(downloadLink);
                },
                error: function () {
                    alert("Cannot export data!");
                }
            });
        }
    }

    function exportInventory() {
        
        if (confirm('Confirm export of inventory balance?')) {
            window.location.href = 'product/export-inventory';
        } else {
            console.log('Export canceled.');
        }
    }

</script>
@endsection
