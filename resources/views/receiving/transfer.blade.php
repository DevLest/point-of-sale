<div class="modal" id="modal-transferID" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <form class="form form-horizontal form-transferId" data-toggle="validator" method="post">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="formclose()" aria-label="Close"><span aria-hidden="true"> &times; </span> </button>
                    <h5 class="modal-title">Transfer ID</h5>
                </div>
                <div class="modal-body">
                    
                    <div class="form-group">
                        <label for="transferId" class="col-md-3 control-label">Transfer ID</label>
                        <div class="col-md-9">
                            <input id="transferId" type="text" maxlength="9" class="form-control" name="transferId" id="transferId" autofocus required placeholder="Please enter transferID from receipt" required>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="origin" class="col-md-3 control-label">Branch Origin</label>
                        <div class="col-md-9">
                            <select class="form-control" id="origin" name="origin" aria-label="Please choose origin Branch" required>
                                @foreach ( App\Branch::where('branch_code','!=',env('BRANCH_CODE'))->get() as $branch )
                                    <option value="{{$branch->id}}">{{$branch->name}}
                                        <small> | {{$branch->location}}</small>
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button onclick="receive()" type="button" class="btn btn-primary">Confirm</button>
                </div>
            </div>
        </form>
    </div>
</div>
