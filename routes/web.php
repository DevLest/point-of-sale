    <?php

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

    use Illuminate\Support\Facades\Route;
    use Illuminate\Support\Facades\Auth;

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home1');

    Auth::routes();

    Route::group(['middleware' => ['web', 'checkAuth']], function(){
        Route::get('user/profile', 'UserController@profile')->name('user.profile');
        Route::patch('user/{id}/change', 'UserController@changeProfile');
        Route::get('user/data', 'UserController@listData')->name('user.data');
        Route::resource('user', 'UserController');

        Route::get('/home/inventory', 'HomeController@listData')->name('home.inventory');

        Route::get('transaction/new', 'SalesDetailController@newSession')->name('transaction.new');
        Route::get('transaction/{id}/data', 'SalesDetailController@listData')->name('transaction.data');
        Route::get('transaction/printnote', 'SalesDetailController@printNote')->name('transaction.print');
        Route::get('transaction/notepdf', 'SalesDetailController@notePDF')->name('transaction.pdf');
        Route::get('transaction/return', 'SalesDetailController@return')->name('transaction.return');
        Route::post('transaction/save', 'SalesDetailController@saveData');
        Route::post('transaction/wholesale', 'SalesDetailController@wholesale');
        Route::post('transaction/admincheck', 'SalesDetailController@admincheck');
        Route::post('transaction/return/{id}', 'SalesDetailController@returnID')->name('transaction.returnID');
        Route::post('transaction/returnConfirm', 'SalesDetailController@returnConfirm')->name('transaction.returnConfirm');
        Route::post('transaction/returnSubmit', 'SalesDetailController@returnSubmit')->name('transaction.returnSubmit');
        Route::post('transaction/addCustomProduct', 'SalesDetailController@addCustomProduct')->name('transaction.addCustomProduct');
        Route::get('transaction/return/{id}/data', 'SalesDetailController@returnData')->name('transaction.returnData');
        Route::get('transaction/loadform/{discount}/{total}/{received}/{id}', 'SalesDetailController@loadForm');
        Route::resource('transaction', 'SalesDetailController');
    });

    Route::group(['middleware' => ['web', 'cekuser:1' ]], function(){
        Route::get('category/data', 'CategoryController@listData')->name('category.data');
        Route::resource('category', 'CategoryController');

        Route::get('product/data', 'ProductController@listData')->name('product.data');
        Route::get('product/dataReceived', 'ProductController@listDataReceived')->name('product.data.received');
        Route::get('product/dataTransfered', 'ProductController@listDataTransfered')->name('product.data.transfered');
        Route::post('product/delete', 'ProductController@deleteSelected');
        Route::post('product/print', 'ProductController@printBarcode');
        Route::post('product/upload', 'ProductController@uploadProducts')->name('product.upload');
        Route::post('product/export', 'ProductController@exportProducts')->name('product.export');
        Route::get('product/export-inventory', 'ProductController@exportInventory')->name('product.export-inventory');
        Route::resource('product', 'ProductController');

        Route::get('supplier/data', 'SupplierController@listData')->name('supplier.data');
        Route::resource('supplier', 'SupplierController');

        Route::get('member/data', 'MemberController@listData')->name('member.data');
        Route::post('member/print', 'MemberController@printCard');
        Route::resource('member', 'MemberController');

        Route::get('expenditure/data', 'ExpenditureController@listData')->name('expenditure.data');
        Route::resource('expenditure', 'ExpenditureController');

        Route::get('purchase/data', 'PurchaseController@listData')->name('purchase.data');
        Route::get('purchase/{id}/add', 'PurchaseController@create');
        Route::get('purchase/{id}/lihat', 'PurchaseController@show');
        Route::resource('purchase', 'PurchaseController');   

        Route::get('purchase_detail/{id}/data', 'PurchaseDetailController@listData')->name('purchase_detail.data');
        Route::get('purchase_detail/loadform/{diskon}/{total}', 'PurchaseDetailController@loadForm');
        Route::resource('purchase_detail', 'PurchaseDetailController');   

        Route::get('sales/data', 'SalesController@listData')->name('sales.data');
        Route::get('sales/dataReturn', 'SalesController@listDataReturn')->name('sales.dataReturn');
        Route::get('sales/{id}/show', 'SalesController@show');
        Route::resource('sales', 'SalesController');

        Route::get('reporting', 'ReportingController@index')->name('reporting.index');
        Route::post('reporting', 'ReportingController@refresh')->name('reporting.refresh');
        Route::get('reporting/data/{start}/{end}', 'ReportingController@listData')->name('reporting.data'); 
        Route::get('reporting/pdf/{start}/{end}', 'ReportingController@exportPDF');

        Route::resource('setting', 'SettingController');

        Route::get('receiving/data', 'ReceivingController@listData')->name('receiving.data');
        Route::post('receiving/new', 'ReceivingController@newSession')->name('receiving.new');
        Route::post('receiving/clear', 'ReceivingController@clearSession')->name('receiving.clear');
        Route::resource('receiving', 'ReceivingController');

        Route::get('transfer/data', 'TransferController@listData')->name('transfer.data');
        Route::get('transfer/newData', 'TransferController@listNewData')->name('transfer.newData');
        Route::post('transfer/new', 'TransferController@newSession')->name('transfer.new');
        Route::get('transfer/notepdf', 'TransferController@notePDF')->name('transfer.pdf');
        Route::any('transfer/clearSession', 'TransferController@clearSession')->name('transfer.clearSession');
        Route::resource('transfer', 'TransferController');

        Route::get('branch/data', 'BranchController@listData')->name('branch.data');
        Route::resource('branch', 'BranchController');
    });

