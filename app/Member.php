<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table = 'member';
	protected $primaryKey = 'member_id';

	public function sales(){
        return $this->hasMany('App\Sales', 'supplier_id');
    }
}
