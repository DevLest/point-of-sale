<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receiving extends Model
{
    protected $table = 'received';
    protected $primaryKey = 'id';
    public $timestamp = true;
}
