<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Returns extends Model
{
    protected $table = 'return';
    protected $primaryKey = 'id';
    public $timestamp = true;
}
