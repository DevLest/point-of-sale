<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    protected $table = 'sales';
	protected $primaryKey = 'sales_id'; 
    
    protected $fillable = [
        'member_code',
        'total_item',
        'total_price',
        'discount',
        'pay',
        'received',
        'user_id',
    ];
}
