<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesDetail extends Model
{
    protected $table = 'sales_detail';
	protected $primaryKey = 'sales_detail_id';
}
