<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        return view('setting.index');
    }

    public function edit($id)
    {
        $setting = Setting::find($id);
        echo json_encode($setting);
    }

    public function update(Request $request, $id)
    {
        $setting = Setting::find($id);
        $setting->store_name   = $request['name'];
        $setting->address         = $request['address'];
        $setting->phone         = $request['phone'];
        // $setting->tipe_nota         = $request['tipe_nota'];
        
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $nama_gambar = "logo.".$file->getClientOriginalExtension();
            $lokasi = public_path('images');

            $file->move($lokasi, $nama_gambar);
            $setting->logo         = $nama_gambar;  
        }

        // if ($request->hasFile('kartu_member')) {
        //     $file = $request->file('kartu_member');
        //     $nama_gambar = "card.".$file->getClientOriginalExtension();
        //     $lokasi = public_path('images');

        //     $file->move($lokasi, $nama_gambar);
        //     $setting->kartu_member   = $nama_gambar;  
        // }
        $setting->update();
    }
}
