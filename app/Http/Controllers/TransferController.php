<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transfer;
use App\TransferDetails;
use App\Product;
use App\Inventory;
use App\Setting;
use Redirect;
use Auth;

class TransferController extends Controller
{
    public function index()
    {
        return view('transfer.index');
    }
 
    public function newSession(Request $request)
    {
        $continueMsg = 0;
        if ( !session()->has('transferId') )
        {
            $transfer = new Transfer;   
            $transfer->to_branch_id = $request->get('target'); 

            $transfer->save();
        }
        else
        {
            $continueMsg = 1;
            $transfer = Transfer::where(['id' => session('transferId'), 'to_branch_id' => (int) $request->get('target')])->first();
            
            if (!$transfer)
            {
                $continueMsg = 0;

                $transfers = Transfer::find(session('transferId'));
                if ($transfers)
                {
                    $transfers->delete();
            
                    TransferDetails::where('transfer_id', session('transferId'))->delete();
                }

                $transfer = new Transfer;
                $transfer->to_branch_id = $request->get('target'); 
    
                $transfer->save();
            }
        }
        
        session(['transferId' => $transfer->id]);
        $transfer_id = session('transferId');
        $product = Inventory::where('stock', '>', 0)->where('branch_code', env('BRANCH_CODE'))->get();

        return view('transfer.transfer', compact('transfer', 'product', 'transfer_id', 'continueMsg'));
    }
 
    public function clearSession(Request $request)
    {
        $transfers = Transfer::find(session('transferId'));
        $transfers->delete();

        TransferDetails::where('transfer_id', session('transferId'))->delete();

        session()->forget('transferId');
        
        return response("Bad");
    }

    public function listData()
    {
        $receiving = Transfer::select('transfer.*', 'branch.name')->leftJoin('branch', 'branch.id', '=', 'transfer.to_branch_id')->where('confirmed', 1)->get();
        $data = array();

        foreach($receiving as $list)
        {
            $transfer = TransferDetails::where('transfer_id', $list->id);
            $row = array();
            $row[] = $list->id;
            $row[] = $transfer->count('product_code');
            $row[] = $transfer->sum('quantity');
            $row[] = $list->name;
            $row[] = date("D, M j, Y h:i A", strtotime($list->created_at));
            $row[] = '<div class="btn-group">
                    <a onclick="showDetail('.$list->id.')" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                    </div>';
            $data[] = $row;
        }
 
        $output = array("data" => $data);
        return response()->json($output);
    }
    
    public function show($id)
    {
        $receiving = TransferDetails::select('transfer_details.*','product.product_name','inventory.price')->leftJoin('product', 'product.product_code', '=', 'transfer_details.product_code')->leftJoin('inventory', 'product.product_code', '=', 'inventory.product_code')->where('transfer_details.transfer_id', $id)->get();
        $data = array();

        foreach($receiving as $list)
        {
            $row = array();
            $row[] = $list->transfer_id;
            $row[] = $list->product_code;
            $row[] = $list->product_name;
            $row[] = "PhP ".$list->price;
            $row[] = $list->quantity;
            $row[] = date("D, M j, Y h:i A", strtotime($list->created_at));
            $data[] = $row;
        }
 
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function listNewData()
    {
        $receiving = TransferDetails::select('transfer_details.*','product.product_name','inventory.price','inventory.stock')->leftJoin('product', 'product.product_code', '=', 'transfer_details.product_code')->leftJoin('inventory', 'product.product_code', '=', 'inventory.product_code')->where('transfer_details.transfer_id', session('transferId'))->get();
        $data = array();

        foreach($receiving as $list)
        {
            $row = array();
            $row[] = $list->product_code;
            $row[] = $list->product_name;
            $row[] = $list->price;
            $row[] = "<input type='number' class='form-control' name='total_$list->id' value='$list->quantity' min='1' max='$list->stock' onChange='changeCount($list->id)'>";
            $row[] = $list->stock;
            $row[] = '  <div class="form-group">
                            <div class="btn-group col-sm-6">
                                <a onclick="deleteItem('.$list->id.')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Remove Product">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>
                        </div>';
            $data[] = $row;
        }
 
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function store(Request $request)
    {
        $product = Inventory::where('product_code', $request['code'])->where('stock', '>', 0)->where('branch_code', env('BRANCH_CODE'))->first();

        if ( $product )
        {
            $detail = TransferDetails::where(['transfer_id' => $request['transferIdvalue'], 'product_code' => $request['code'] ])->first();
            
            if ( $detail )
            {
                $detail->quantity += 1;
            }
            else
            {
                $detail = new TransferDetails;
                $detail->transfer_id = $request['transferIdvalue'];
                $detail->product_code = $request['code'];
                $detail->quantity = 1;
            }
            
            if($detail->save())
            {
                return response("Good");
            }
            return response("Bad");
        }
        else
        {
            return response("Bad");
        }
    }

    public function edit($id)
    {
        $receiving = Transfer::select('transfer.*','branch.name')->leftJoin('branch', 'branch.id', '=', 'transfer.to_branch_id')->where('transfer.id',$id)->first();
        $details ="";

        $receiving->confirmed = 1;
        $receiving->total_items = TransferDetails::where(['transfer_id' => $receiving->id])->count();

        if ($receiving->save())
        {
            $details = TransferDetails::select('transfer_details.*','product.product_name')->leftJoin('product', 'product.product_code', '=', 'transfer_details.product_code')->where(['transfer_details.transfer_id' => $receiving->id])->get();
            
            foreach($details as $detail)
            {
                $product = Inventory::where('product_code', $detail->product_code)->where('stock', '>', 0)->where('branch_code', env('BRANCH_CODE'))->first();

                $product->stock -= $detail->quantity;
                $product->save();
            }
        }
    }

    public function update(Request $request, $id)
    {
        $input_name = "total_".$id;
        $detail = TransferDetails::find($id);

        $detail->quantity = $request[$input_name];
        $detail->update();
    }

    public function destroy($id)
    {
        $detail = TransferDetails::find($id);
        $detail->delete();
    }

    public function notePDF(){
        $details = TransferDetails::select('transfer_details.*','product.product_name')->leftJoin('product', 'product.product_code', '=', 'transfer_details.product_code')->where(['transfer_details.transfer_id' => session('transferId')])->get();

        $receiving = Transfer::select('transfer.*','branch.branch_code as name')->leftJoin('branch', 'branch.id', '=', 'transfer.to_branch_id')->where('transfer.id',session('transferId'))->first();
        $setting = Setting::find(1);
    
        session()->forget('transferId');
        return view('transfer.notepdf', compact('details', 'receiving', 'setting'));
    }
}
