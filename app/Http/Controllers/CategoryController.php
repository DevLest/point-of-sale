<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('category.index'); 
    }

    public function listData()
    {
        $user = Auth::user();
    
        $category = Category::orderBy('category_id', 'desc')->get();
        $no = 0;
        $data = array();
        foreach($category as $list){
        $no ++;
        $buttons = '<div class="btn-group">
        <a onclick="editForm('.$list->category_id.')" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';

        if ($user->level == 0) {
            $buttons .= '<a onclick="deleteData('.$list->category_id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>';
        }

        $row = array();
        $row[] = $no;
        $row[] = $list->category;
        $row[] = $buttons.'</div>';
        $data[] = $row;
        }

        $output = array("data" => $data);
        return response()->json($output);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category;
        $category->category = $request['name'];
        $category->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return json_encode($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $category = Category::find($id);
        $category->category = $request['name'];
        $category->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
    }
}
