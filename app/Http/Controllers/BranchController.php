<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Branch;

class BranchController extends Controller
{
    public function index()
    {
        return view('branch.index'); 
    }

    public function listData()
    {
        $branch = Branch::all();
        $data = array();
        foreach($branch as $list)
        {
            $row = array();
            $row[] = $list->name;
            $row[] = $list->location;
            $row[] = $list->branch_code;
            $data[] = $row;
        }

        $output = array("data" => $data);
        return response()->json($output);
    }
    
    public function create()
    {
    }
    
    public function store(Request $request)
    {
    }
    
    public function show(Branch $branch)
    {
    }
    
    public function edit($id)
    {
    }
    
    public function update(Request $request, $id)
    {
    }
    
    public function destroy($id)
    {
    }
}
