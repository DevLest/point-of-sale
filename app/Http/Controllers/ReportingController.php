<?php

namespace App\Http\Controllers;

use App\Purchase;
use App\Sales;
use App\Expenditure;
use Illuminate\Http\Request;

class ReportingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $start = date('Y-m-d', mktime(0,0,0, date('m'), 1, date('Y')));
        $end = date('Y-m-d');
        return view('report.index', compact('start', 'end'));   
    }

    protected function getData($start, $end){
        $no = 0;
        $data = array();
        $revenue = 0;
        $total_revenue = 0;
        while(strtotime($start) <= strtotime($end)){
            $date = $start;
            $start = date('Y-m-d', strtotime("+1 day", strtotime($start)));

            $total_sales = Sales::where('created_at', 'LIKE', "$date%")->sum('pay');
            $total_purchase = Purchase::where('created_at', 'LIKE', "$date%")->sum('pay');
            $total_expenditure = Expenditure::where('created_at', 'LIKE', "$date%")->sum('denomination');

            $revenue = $total_sales - $total_purchase - $total_expenditure;
            $total_revenue += $revenue;

            $no ++;
            $row = array();
            $row[] = $no;
            $row[] = $date;
            $row[] = format_money($total_sales);
            $row[] = format_money($total_purchase);
            $row[] = format_money($total_expenditure);
            $row[] = format_money($revenue);
            $data[] = $row;
        }
        $data[] = array("", "", "", "", "Total Revenue", format_money($total_revenue));

        return $data;
    }

    public function listData($start, $end)
    {   
        $data = $this->getData($start, $end);

        $output = array("data" => $data);
        return response()->json($output);
    }

    public function refresh(Request $request)
    {
        $start = $request['start'];
        $end = $request['end'];
        return view('report.index', compact('start', 'end')); 
    }

    public function exportPDF($start, $end){
        $date_start = $start;
        $date_end = $end;
        $data = $this->getData($start, $end);

        $pdf = \PDF::loadView('report.pdf', compact('start', 'end', 'data'));
        $pdf->setPaper('a4', 'potrait');
        
        return $pdf->stream();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
