<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Setting;
use App\Category;
use App\Product;
use App\Supplier;
use App\Member;
use App\Sales;
use App\SalesDetail;
use Charts;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $setting = Setting::where('branch_code', env('BRANCH_CODE'))->first();

        if(in_array(Auth::user()->level , [1,0]) )
        {
            $sales_today_product = [];
            $sales_today_amount = [];
            $sales_today_color = [];
            $sales_today_color1 = [];

            $week_date_data = [];
            $week_sales_data = [];

            $month_sales_query = Sales::where('branch_code', $setting->branch_code)->where('created_at', '>', date('Y-m-01 00:00:00'))->where('created_at', '<=', date('Y-m-t 23:59:59'));
            $month_sales_detail_query = SalesDetail::select('sales_detail.*','product.product_name', 'category.category')->leftJoin('product', 'product.product_code', '=', 'sales_detail.product_code')->leftJoin('category', 'product.category_id', '=', 'category.category_id')->leftJoin('sales', 'sales.sales_id', '=', 'sales_detail.sales_id')->where('sales_detail.created_at', '>', date('Y-m-01 00:00:00'))->where('sales_detail.created_at', '<=', date('Y-m-t 23:59:59'))->where('sales.branch_code', $setting->branch_code);
        }
        
        $today_sales = Sales::where('branch_code', $setting->branch_code)->where('created_at', '>', date('Y-m-d 00:00:00'))->where('created_at', '<=', date('Y-m-d 23:59:59'));
        
        if(in_array(Auth::user()->level , [1,0]) )
        {
            $month_sales = $month_sales_query;
            $last_month_sales = Sales::where('branch_code', $setting->branch_code)->where('created_at', '>', date("Y-m-d 00:00:00", strtotime("first day of previous month")))->where('created_at', '<=', date("Y-m-d 00:00:00", strtotime("last day of previous month")));
            $annual_sales = Sales::where('branch_code', $setting->branch_code)->where('created_at', '>', date('Y-01-01 00:00:00'))->where('created_at', '<=', date('Y-12-31 23:59:59'));
            
            $query_today_sales = SalesDetail::select('sales_detail.*','product.product_name')->leftJoin('sales', 'sales.sales_id', '=', 'sales_detail.sales_id')->leftJoin('product', 'product.product_code', '=', 'sales_detail.product_code')->where('sales_detail.created_at', '>', date('Y-m-d 00:00:00'))->where('sales_detail.created_at', '<=', date('Y-m-d 23:59:59'))->where('sales.branch_code', $setting->branch_code)->orderBy('sales_detail.sales_id', 'desc')->get();

            foreach ( $query_today_sales as $sales_today)
            {
                $sales_today_color[] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
                $sales_today_color1[] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));

                $product_name = ($sales_today->product_name != null && $sales_today->product_name != "") ? $sales_today->product_name : $sales_today->product_code;

                if (isset($sales_today_product[$product_name])) $sales_today_product[$product_name] += $sales_today->sub_total;
                else $sales_today_product[$product_name] = $sales_today->sub_total;

                if (isset($sales_today_amount[$sales_today->sales_id])) $sales_today_amount[$sales_today->sales_id] += $sales_today->sub_total;
                else $sales_today_amount[$sales_today->sales_id] = $sales_today->sub_total;
            }

            arsort($sales_today_product);
            $sales_today_product = array_slice($sales_today_product, 0, 10, true);

            arsort($sales_today_amount);
            $sales_today_amount = array_slice($sales_today_amount, 0, 10, true);
            $sales_today_color = array_slice($sales_today_color, 0, 10);
            $sales_today_color1 = array_slice($sales_today_color1, 0, 10);

            $salesChart = app()->chartjs
                    ->name('TodaysTopSales')
                    ->type('doughnut')
                    ->size(['width' => 400, 'height' => 200])
                    ->labels(array_keys($sales_today_product))
                    ->datasets([
                        [
                            "label" => "Today's 10 Top Sales per Product",
                            'backgroundColor' => $sales_today_color,
                            'hoverBackgroundColor' => $sales_today_color,
                            'data' => array_values($sales_today_product),
                        ]
                    ])
                    ->options([]);

            $quantityChart = app()->chartjs
                    ->name('TodaysTopQuanitity')
                    ->type('doughnut')
                    ->size(['width' => 400, 'height' => 200])
                    ->labels(array_keys($sales_today_amount))
                    ->datasets([
                        [
                            "label" => "Today's Top 10 Sold Transactions",
                            'backgroundColor' => $sales_today_color1,
                            'hoverBackgroundColor' => $sales_today_color1,
                            'data' => array_values($sales_today_amount),
                        ]
                    ])
                    ->options([]);

            $startWeek = date('Y-m-d', strtotime('monday this week'));
            $endWeek = date('Y-m-d', strtotime('sunday this week'));
            $date = $startWeek;

            while(strtotime($date) <= strtotime($endWeek)){ 
                $day = substr($date,8,2);
                $date_data[] = $day.date(", l", strtotime(date("Y-".substr($date,5,2)."-$day")));
                
                $sales = Sales::where('created_at', 'LIKE', "$date%")->sum('pay');
                $week_sales_data[] = (int) $sales;

                $date = date('Y-m-d', strtotime("+1 day", strtotime($date)));
            }

            $weeklyChart = app()->chartjs
                    ->name('WeeklySales')
                    ->type('line')
                    ->size(['width' => 400, 'height' => 150])
                    ->labels($week_date_data)
                    ->datasets([
                        [
                            "label" => "Weekly Sales Data",
                            'backgroundColor' => "rgba(38, 185, 154, 0.31)",
                            'borderColor' => "rgba(38, 185, 154, 0.7)",
                            "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                            "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                            "pointHoverBackgroundColor" => "#fff",
                            "pointHoverBorderColor" => "rgba(220,220,220,1)",
                            'data' => $week_sales_data,
                        ]
                    ])
                    ->options([]);

            $sales_month_product = [];
            $sales_month_category = [];
            $sales_month_color = [];
            $sales_month_color1 = [];
            $month_sales_data = $month_sales_detail_query->get();
            
            foreach ( $month_sales_data as $sales_month)
            {
                $sales_month_color[] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
                $sales_month_color1[] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));

                $product_name = ($sales_month->product_name != null && $sales_month->product_name != "") ? $sales_month->product_name : $sales_month->product_code;

                if (isset($sales_month_product[$product_name])) $sales_month_product[$product_name] += $sales_month->sub_total;
                else $sales_month_product[$product_name] = $sales_month->sub_total;

                $category_name = ($sales_month->category != null && $sales_month->category != "") ? $sales_month->category : "Uncategorize Products";

                if (isset($sales_month_category[$category_name])) $sales_month_category[$category_name] += $sales_month->sub_total;
                else $sales_month_category[$category_name] = $sales_month->sub_total;
            }

            arsort($sales_month_product);
            $sales_month_product = array_slice($sales_month_product, 0, 20, true);
            $sales_month_product_keys = array_keys($sales_month_product);

            arsort($sales_month_category);
            $sales_month_category = array_slice($sales_month_category, 0, 20, true);
            $sales_month_category_keys = array_keys($sales_month_category);

            $sales_month_color = array_slice($sales_month_color, 0, count($sales_month_category_keys));
            $sales_month_color1 = array_slice($sales_month_color1, 0, count($sales_month_product_keys));
            
            $categoryChart = app()->chartjs
                    ->name('TopCategory')
                    ->type('polarArea')
                    ->size(['width' => 400, 'height' => 200])
                    ->labels($sales_month_category_keys)
                    ->datasets([
                        [
                            "label" => "Top Categories of the Month",
                            'backgroundColor' => $sales_month_color,
                            'hoverBackgroundColor' => $sales_month_color,
                            'data' => array_values($sales_month_category),
                        ]
                    ])
                    ->options([]);

            $productChart = app()->chartjs
                    ->name('TopProduct')
                    ->type('pie')
                    ->size(['width' => 400, 'height' => 150])
                    ->labels($sales_month_product_keys)
                    ->datasets([
                        [
                            "label" => "Top Products of the Month",
                            'backgroundColor' => $sales_month_color1,
                            'hoverBackgroundColor' => $sales_month_color1,
                            'data' => array_values($sales_month_product),
                        ]
                    ])
                    ->options([]);

        }
        else 
        {
            $today_sales = $today_sales->where('user_id', Auth::user()->id);
        }

        $today_sales_net = $today_sales->sum('net');
        $today_sales = $today_sales->sum('pay');
            
        if(in_array(Auth::user()->level , [1,0]) )
        {
            $month_sales_net = $month_sales->sum('net');
            $month_sales = $month_sales->sum('pay');
            $last_month_sales_net = $last_month_sales->sum('net');
            $last_month_sales = $last_month_sales->sum('pay');
            $annual_sale_net = $annual_sales->sum('net');
            $annual_sales = $annual_sales->sum('pay');

            return view('home.admin', compact('today_sales_net', 'month_sales_net', 'last_month_sales_net', 'annual_sale_net', 'last_month_sales', 'annual_sales', 'today_sales', 'month_sales', 'annual_sales', 'salesChart', 'quantityChart', 'weeklyChart', 'categoryChart', 'productChart'));
        }
        else return view('home.cashier', compact('setting', 'today_sales'));
    }

    public function listData()
    {
        $product = Product::select('product.*', 'category.category', 'inventory.stock')->leftJoin('category', 'category.category_id', '=', 'product.category_id')->leftJoin('inventory', 'inventory.product_code', '=', 'product.product_code')->where('inventory.branch_code', env('BRANCH_CODE'))->orderBy('product.product_id', 'desc')->get();
        
        $data = [];
        foreach($product as $list)
        {
            if ($list->stock <= $list->stock_limit)
            {
                $row = [];
                $row[] = $list->product_code;
                $row[] = $list->product_name;
                $row[] = $list->stock;
                $data[] = $row;
            }
        }
        
        return response()->json(["data" => $data]);
    }
}
