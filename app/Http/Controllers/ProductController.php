<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Support\Facades\Auth;
use PDF;
use App\Product;
use App\Inventory;
use App\ReceivingDetails;
use App\TransferDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::all();    
        $user = Auth::user();  
        return view('product.index', compact('category','user'));
    }

    public function listData()
    {
        $user = Auth::user();
        $product = Product::select('product.*', 'category.category', 'inventory.cost', 'inventory.discount', 'inventory.price', 'inventory.wholesaleprice', 'inventory.stock')->leftJoin('category', 'category.category_id', '=', 'product.category_id')->leftJoin('inventory', 'inventory.product_code', '=', 'product.product_code')->orderBy('product.created_at', 'desc')->get();
        
        $no = 0;
        $data = array();
        foreach($product as $list){
            $no ++;
            $row = array();
            $buttons = "<div class='btn-group'>
            <a onclick='editForm(".$list->product_id.")' class='btn btn-primary btn-sm'><i class='fa fa-pencil'></i></a>";
            
            if ($user->level == 0) {
                $buttons .= "<a onclick='deleteData(".$list->product_id.")' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>";
            }
            
            $row[] = "<input type='checkbox' name='id[]'' value='".$list->product_id."'>";
            $row[] = $no;
            $row[] = $list->product_code;
            $row[] = $list->product_name;
            $row[] = $list->category;
            $row[] = $list->brand;
            $row[] = "PhP ".format_money($list->cost);
            $row[] = "PhP ".format_money($list->price);
            $row[] = "PhP ".format_money($list->wholesaleprice);
            $row[] = $list->stock;
            $row[] = $list->stock_limit;
            $row[] = $buttons.
                    "</div>";
            $data[] = $row;
        }
        
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function listDataReceived()
    {
        $product = ReceivingDetails::select('receive_details.*', 'product.product_name', 'product.brand', 'branch.name', 'received.from_branch_id')->leftJoin('product', 'receive_details.product_code', '=', 'product.product_code')->leftJoin('category', 'category.category_id', '=', 'product.category_id')->leftJoin('received', 'received.id', '=', 'receive_details.receive_id')->leftJoin('branch', 'received.from_branch_id', '=', 'branch.id')->orderBy('receive_details.updated_at', 'desc')->get();
        
        $no = 0;
        $data = [];
        foreach($product as $list){
            $row = [];
            $row[] = $list->product_code;
            $row[] = $list->product_name;
            $row[] = $list->category;
            $row[] = $list->brand;
            $row[] = $list->name;
            $row[] = $list->quantity;
            $data[] = $row;
        }
        
        return response()->json(["data" => $data]);
    }

    public function listDataTransfered()
    {
        $product = TransferDetails::select('transfer_details.*', 'product.product_name', 'product.brand', 'branch.name', 'transfer.to_branch_id')->leftJoin('product', 'transfer_details.product_code', '=', 'product.product_code')->leftJoin('category', 'category.category_id', '=', 'product.category_id')->leftJoin('transfer', 'transfer.id', '=', 'transfer_details.transfer_id')->leftJoin('branch', 'transfer.to_branch_id', '=', 'branch.id')->orderBy('transfer_details.updated_at', 'desc')->get();
        
        $no = 0;
        $data = [];
        foreach($product as $list){
            $row = [];
            $row[] = $list->product_code;
            $row[] = $list->product_name;
            $row[] = $list->category;
            $row[] = $list->brand;
            $row[] = $list->name;
            $row[] = $list->quantity;
            $data[] = $row;
        }
        
        return response()->json(["data" => $data]);
    }

    public function store(Request $request)
    {
        $product = Product::where('product_code', '=', $request['code'])->count();
        if($product < 1){
            $product = new Product;
            $product->product_code   = $request['code'];
            $product->product_name   = $request['name'];
            $product->category_id    = $request['category'];
            $product->brand          = (isset($request['brand'])) ? $request['brand'] : " ";
            $product->stock_limit    = $request['stock_limit'];
            $product->save();

            $inventory = new Inventory;
            $inventory->product_code   = $request['code'];
            $inventory->cost           = $request['cost'];
            $inventory->wholesaleprice = $request['wholesaleprice'];
            $inventory->price          = $request['price'];
            $inventory->stock          = $request['stock'];
            $inventory->branch_code = env('BRANCH_CODE');
            $inventory->save();

            echo json_encode(array('msg'=>'success'));
        }else{
            echo json_encode(array('msg'=>'error'));
        }
    }
    
    public function edit($id)
    {
        $product = Product::select('product.*', 'inventory.stock', 'inventory.wholesaleprice', 'inventory.cost', 'inventory.price' )->leftJoin('inventory', 'inventory.product_code', '=', 'product.product_code')->where('product.product_id',$id)->first();
        echo json_encode(['data' => $product, 'user' => Auth::user()]);
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $product->product_name   = $request['name'];
        $product->category_id    = $request['category'];
        $product->brand          = $request['brand'];
        $product->stock_limit    = $request['stock_limit'];
        $product->update();

        $inventory = Inventory::where('product_code', $product->product_code)->first();
        $inventory->cost           = $request['cost'];
        $inventory->wholesaleprice = $request['wholesaleprice'];
        $inventory->price          = $request['price'];
        $inventory->stock          = $request['stock'];
        $inventory->update();

        echo json_encode(array('msg'=>'success'));
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        
        if (!$product)
        {
            $inventory = Inventory::where('product_code', $product->product_code)->first();
        
            if (!$inventory)
            {
                $inventory->delete();
            }
        }

        $product->delete();
    }

    public function deleteSelected(Request $request)
    {
        foreach($request['id'] as $id){
            $product = Product::find($id);
            $product->delete();
        }
    }

    public function show(Request $request){}

    public function printBarcode(Request $request)
    {
        $dataproduct = array();
        foreach($request['id'] as $id){
            $product = Product::select('product.*', 'inventory.*' )->leftJoin('inventory', 'inventory.product_code', '=', 'product.product_code')->where('product_id', $id)->first();
            $dataproduct[] = $product;
        }
        $no = 1;
        $pdf = PDF::loadView('product.barcode', compact('dataproduct', 'no'));
        $pdf->setPaper('a4', 'potrait');      
        return $pdf->stream();
    }

    public function uploadProducts(Request $request)
    {
        $file_handle = fopen($request->file('fileupload')->path(), 'r');
        $count = 0;
        $errorCount = -1;

        while (!feof($file_handle)) 
        {
            $values = fgetcsv($file_handle, 0, ",");

            if (!empty($values))
            {
                if ($values <= 1) 
                {
                    return redirect()->route('product.index', ["error" => "File format not allowed | File should be CSV (Comma delimeted)", "success" => 0] );
                }

                $product_code = ltrim(ltrim(is_numeric($values[0]) ? number_format($values[0],0,'','') : $values[0] ,"0")," ");
                $title = ltrim($this->slug($values[1])," ");
                $description = ltrim($values[2]," ");
                $brand = ltrim(strtoupper($values[3])," ");
                $category = ltrim(strtoupper($values[4])," ");
                $cost = floatval($values[5]);
                $price = floatval($values[6]);
                $wholesale = floatval( (isset($values[7]) && $values[7] > 0) ? $values[7] : $values[6] );
                $stock = floatval( (isset($values[8]) && $values[8] > 0) ? $values[8] : 0 );

                try 
                {
                    if ( $product_code != "" && $title != "" && $category != "" && $cost > 0 && $price > 0 )
                    {
                        $categoryData = Category::where('category', $category)->first();

                        if (!$categoryData)
                        {
                            $categoryData = new Category;
                            $categoryData->category = $category;
                            $categoryData->save();
                        }

                        $product = Product::where('product_code',$product_code)->first();

                        if (!$product)
                        {
                            $product = new Product;
                        }

                        $product->product_code = $product_code;
                        $product->category_id = $categoryData->category_id;
                        $product->product_name = $title;
                        $product->brand = $brand;
                        $product->save();
                        
                        $inventory = Inventory::where('product_code',$product_code)->where('branch_code', env('BRANCH_CODE'))->first();

                        if (!$inventory)
                        {
                            $inventory = new Inventory;
                        }
                        
                        $inventory->product_code = $product_code;
                        $inventory->cost = $cost;
                        $inventory->price = $price;
                        $inventory->wholesaleprice = $wholesale;
                        $inventory->branch_code = env('BRANCH_CODE');

                        if ($stock > 0) $inventory->stock = $stock;
                        
                        $inventory->save();

                        $count++;
                    }
                    else
                    {
                        $errorCount++;
                    }
                } 
                catch (\Illuminate\Database\QueryException $e){continue;}
                catch (\PDOException $e){continue;}
                catch (\Exception $e){continue;}
                catch (\Throwable $e){continue;}
            }
        }
        fclose($file_handle);

        return redirect()->route('product.index', ["error" => $errorCount, "success" => $count] );
    }

    private function slug($string){
        $string = str_replace(' ', '-', $string);
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return str_replace('-', ' ', $string);
    }

    public function exportProducts(Request $request)
    {
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="product_export_'.date('Y_m_d-H_s_m', time()).'.csv";');

        $file = fopen('php://output', 'w');
            
        fputcsv($file, [
            "Prodoct Code",
            "Prodoct Name",
            "Category",
            "Brand",
            "Cost",
            "Retail price",
            "Wholesale price",
            // "In-stock",
            // "Branch Code",
        ]);
        
        foreach($request['id'] as $id){
            $product = Product::select('product.*', 'inventory.*', 'category.category' )->leftJoin('inventory', 'inventory.product_code', '=', 'product.product_code')->leftJoin('category', 'category.category_id', '=', 'product.category_id')->where('product_id', $id)->first();
            
            fputcsv($file, [
                $product->product_code,
                $product->product_name,
                $product->category,
                $product->brand,
                $product->cost,
                $product->price,
                $product->wholesaleprice,
                // $product->stock,
                // $product->branch_code,
            ]);
        }
        fclose($file);
    }
    
    public function exportInventory(Request $request)
    {        
        $products = Product::select('product.*', 'inventory.*', 'category.category' )->leftJoin('inventory', 'inventory.product_code', '=', 'product.product_code')->leftJoin('category', 'category.category_id', '=', 'product.category_id')->where('inventory.stock', '>', 0)->get();   

        $filename = 'inventory_export_'.date('Y_m_d-H_s_m', time()).'.csv';
        $handle = fopen($filename, 'w+');

        fputcsv($handle, [
            "Prodoct Code",
            "Prodoct Name",
            "Category",
            "Brand",
            "Cost",
            "Price",
            "In-stock",
        ]);

        foreach($products as $product){
            fputcsv($handle, [
                $product->product_code,
                $product->product_name,
                $product->category,
                $product->brand,
                $product->cost,
                $product->price,
                $product->stock,
            ]);
        }

        fclose($handle);

        return Response::download($filename)->deleteFileAfterSend(true);
    }
}
