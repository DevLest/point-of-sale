<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Receiving;
use App\ReceivingDetails;
use App\Product;
use App\Inventory;

class ReceivingController extends Controller
{
    public function index()
    {
        $product = Product::all();

        if (str_contains(env('BRANCH_CODE'),'MAIN')) 
        {
            $receiving = new Receiving; 
            $receiving->transfer_id = "MAIN1000000";    
            $receiving->from_branch_id = env('BRANCH_CODE'); 
    
            if ($receiving->save())
            {
                session(['receivedId' => $receiving->id]);
            }
        }

        $transfer_id = session('receivedId');

        return view('receiving.index', compact('product', 'transfer_id'));
    }
 
    public function newSession(Request $request)
    {
        $receiving = new Receiving; 
        $receiving->transfer_id = $request->get('transferId');    
        $receiving->from_branch_id = $request->get('origin'); 

        if ($receiving->save())
        {
            session(['receivedId' => $receiving->id]);
            return response($receiving->id);
        } 
        
        return response("Bad");
    }
 
    public function clearSession(Request $request)
    {
        session()->forget('receivedId');
    }

    public function listData()
    {
        $receiving = ReceivingDetails::select('receive_details.*','product.product_name','inventory.price','inventory.stock')->leftJoin('product', 'product.product_code', '=', 'receive_details.product_code')->leftJoin('inventory', 'product.product_code', '=', 'inventory.product_code')->where('receive_details.receive_id', session('receivedId'))->get();
        $data = array();
        $data = array();

        foreach($receiving as $list)
        {
            $row = array();
            $row[] = $list->product_code;
            $row[] = $list->product_name;
            $row[] = $list->price;
            $row[] = "<input type='number' class='form-control' name='total_$list->id' value='$list->quantity' onChange='changeCount($list->id)'>";
            $row[] = '  <div class="form-group">
                            <div class="btn-group col-sm-6">
                                <a onclick="deleteItem('.$list->id.')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Remove Product">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>
                        </div>';
            $data[] = $row;
        }
 
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function store(Request $request)
    {
        $product = Inventory::where('product_code', $request['code'])->where('branch_code', env('BRANCH_CODE'))->first();

        if ( $product )
        {
            $detail = ReceivingDetails::where(['receive_id' => $request['transferIdvalue'], 'product_code' => $request['code'] ])->first();
            
            if ( $detail )
            {
                $detail->quantity += 1;
            }
            else
            {
                $detail = new ReceivingDetails;
                $detail->receive_id = $request['transferIdvalue'];
                $detail->product_code = $request['code'];
                $detail->quantity = 1;
            }
            
            if($detail->save())
            {
                return response("Good");
            }
            return response("Bad");
        }
        else
        {
            return response("Bad");
        }
    }

    public function edit($id)
    {
        $receiving = Receiving::find($id);

        $receiving->confirmed = 1;

        if ($receiving->save())
        {
            $details = ReceivingDetails::where(['receive_id' => $receiving->id])->get();
            
            foreach($details as $detail)
            {
                $product = Inventory::where('product_code', $detail->product_code)->where('branch_code', env('BRANCH_CODE'))->first();

                $product->stock += $detail->quantity;
                $product->save();
            }
        }
        
        session()->forget('receivedId');
    }

    public function update(Request $request, $id)
    {
        $input_name = "total_".$id;
        $detail = ReceivingDetails::find($id);

        $detail->quantity = $request[$input_name];
        $detail->update();
    }

    public function destroy($id)
    {
        $detail = ReceivingDetails::find($id);
        $detail->delete();
    }
}
