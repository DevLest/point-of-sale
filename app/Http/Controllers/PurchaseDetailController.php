<?php

namespace App\Http\Controllers;

use Redirect;
use App\Purchase;
use App\Supplier;
use App\Product;
use App\PurchaseDetail;
use Illuminate\Http\Request;

class PurchaseDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = Produk::all();
        $idpurchase = session('idpurchase');
        $supplier = Supplier::find(session('idsupplier'));
        return view('purchase_detail.index', compact('produk', 'idpurchase', 'supplier'));
    }

    public function listData($id)
    {

        $detail = PurchaseDetail::leftJoin('produk', 'produk.kode_produk', '=', 'purchase_detail.kode_produk')
            ->where('id_purchase', '=', $id)
            ->get();
        $no = 0;
        $data = array();
        $total = 0;
        $total_item = 0;
        foreach($detail as $list){
            $no ++;
            $row = array();
            $row[] = $no;
            $row[] = $list->kode_produk;
            $row[] = $list->nama_produk;
            $row[] = "Rp. ".format_uang($list->harga_beli);
            $row[] = "<input type='number' class='form-control' name='jumlah_$list->id_purchase_detail' value='$list->jumlah' onChange='changeCount($list->id_purchase_detail)'>";
            $row[] = "Rp. ".format_uang($list->harga_beli * $list->jumlah);
            $row[] = '<a onclick="deleteItem('.$list->id_purchase_detail.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>';
            $data[] = $row;

            $total += $list->harga_beli * $list->jumlah;
            $total_item += $list->jumlah;
        }

        $data[] = array("<span class='hide total'>$total</span><span class='hide totalitem'>$total_item</span>", "", "", "", "", "", "");
        
        $output = array("data" => $data);
        return response()->json($output);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $produk = Produk::where('kode_produk', '=', $request['kode'])->first();
        $detail = new PurchaseDetail;
        $detail->id_purchase = $request['idpurchase'];
        $detail->kode_produk = $request['kode'];
        $detail->harga_beli = $produk->harga_beli;
        $detail->jumlah = 1;
        $detail->sub_total = $produk->harga_beli;
        $detail->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PembelianDetail  $pembelianDetail
     * @return \Illuminate\Http\Response
     */
    public function show(PurchaseDetail $purchaseDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PembelianDetail  $pembelianDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(PurchaseDetail $purchaseDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PembelianDetail  $pembelianDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $nama_input = "price_".$id;
        $detail = PurchaseDetail::find($id);
        $detail->jumlah = $request[$nama_input];
        $detail->sub_total = $detail->harga_beli * $request[$nama_input];
        $detail->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PurchaseDetail  $pembelianDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $detail = PurchaseDetail::find($id);
        $detail->delete();
    }

    public function loadForm($diskon, $total){
        $bayar = $total - ($diskon / 100 * $total);
        $data = array(
            "totalrp" => format_uang($total),
            "bayar" => $pay,
            "bayarrp" => format_uang($pay),
            "terbilang" => ($pay > 0) ? ucwords( numberTowords( $pay ) ) : "" ." Pesos",
        );
        return response()->json($data);
    }
}
