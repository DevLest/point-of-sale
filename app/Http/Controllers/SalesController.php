<?php

namespace App\Http\Controllers;

use Redirect;
use App\Product;
use App\Inventory;
use App\Member;
use App\Sales;
use App\SalesDetail;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    public function index(Request $request)
    {
        $date1 = date('Y-m-d 00:00:00',time());
        $date2 = date('Y-m-d h:i:s',time());
        $o_date1 = $date1;
        $o_date2 = $date2;

        if ($request->date1 !== null && $request->date2 !== null)
        {
            $date1 = date('Y-m-d 00:00:00', strtotime($request->date1) );
            $date2 = date('Y-m-d 23:59:59', strtotime($request->date2) );
            $o_date1 = $date1;
            $o_date2 = $date2;

            // dump($o_date1);
            // dd($o_date2);
        }

        $sales_product_color = [];
        $sales_category_color = [];
        $sales_product = [];
        $sales_category = [];

        $query_today_sales = SalesDetail::select('sales_detail.*', 'product.product_name', 'category.category')->leftJoin('sales', 'sales.sales_id', '=', 'sales_detail.sales_id')->leftJoin('product', 'product.product_code', '=', 'sales_detail.product_code')->leftJoin('category', 'product.category_id', '=', 'category.category_id')->where('sales_detail.created_at', '>', $date1)->where('sales_detail.created_at', '<=', $date2)->where('sales.branch_code', env('BRANCH_CODE'))->orderBy('sales_detail.sales_id', 'desc')->get();
        
        foreach ( $query_today_sales as $sales_today)
        {
            $sales_product_color[] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
            $sales_category_color[] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));

            $product_name = ($sales_today->product_name != null && $sales_today->product_name != "") ? $sales_today->product_name : $sales_today->product_code;

            if (isset($sales_product[$product_name])) $sales_product[$product_name] += $sales_today->sub_total;
            else $sales_product[$product_name] = $sales_today->sub_total;

            if (isset($sales_category[$sales_today->category])) $sales_category[$sales_today->category] += $sales_today->sub_total;
            else $sales_category[$sales_today->category] = $sales_today->sub_total;
        }
        
        $productChart = app()->chartjs
                        ->name('TopProduct')
                        ->type('polarArea')
                        ->size(['width' => 400, 'height' => 200])
                        ->labels(array_keys($sales_product))
                        ->datasets([
                            [
                                "label" => "Top Categories of the Month",
                                'backgroundColor' => $sales_product_color,
                                'hoverBackgroundColor' => $sales_product_color,
                                'data' => array_values($sales_product),
                            ]
                        ])
                        ->options([]);

        $categoryChart = app()->chartjs
                        ->name('TopCategory')
                        ->type('polarArea')
                        ->size(['width' => 400, 'height' => 200])
                        ->labels(array_keys($sales_category))
                        ->datasets([
                            [
                                "label" => "Top Categories of the Month",
                                'backgroundColor' => $sales_category_color,
                                'hoverBackgroundColor' => $sales_category_color,
                                'data' => array_values($sales_category),
                            ]
                        ])
                        ->options([]);

        return view('sales.index', compact('productChart', 'categoryChart', 'o_date1', 'o_date2')); 
    }

    public function listData(Request $request)
    {
        $date1 = date('Y-m-d 00:00:00',time());
        $date2 = date('Y-m-d h:i:s',time());

        if ($request->date1 !== null && $request->date2 !== null)
        {
            $date1 = date('Y-m-d 00:00:00', strtotime($request->date1) );
            $date2 = date('Y-m-d 23:59:59', strtotime($request->date2) );
        }
        
        $sales = Sales::leftJoin('users', 'users.id', '=', 'sales.user_id')
                ->select('users.*', 'sales.*', 'sales.created_at as date')
                ->where('sales.created_at', '>=', $date1)
                ->where('sales.created_at', '<=', $date2)
                ->where('sales.pay', '>', 0)
                ->orderBy('sales.sales_id', 'desc')
                ->get();

        $no = 0;
        $data = array();

        foreach($sales as $list)
        {
            $no ++;
            $row = array();
            $row[] = $no;
            $row[] = date( "F j, Y, g:i a",strtotime($list->created_at));
            $row[] = $list->sales_id;
            $row[] = $list->total_item;
            $row[] = "PhP. ".format_money($list->total_price);
            $row[] = $list->diskon."%";
            $row[] = "PhP. ".format_money($list->pay);
            $row[] = $list->branch_code;
            $row[] = $list->name;
            $row[] = '<div class="btn-group">
                    <a onclick="showDetail('.$list->sales_id.')" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                    </div>';
            $data[] = $row;
        }

        $output = array("data" => $data);
        return response()->json($output);
    }

    public function listDataReturn(Request $request)
    {
        $date1 = date('Y-m-d 00:00:00',time());
        $date2 = date('Y-m-d h:i:s',time());

        if ($request->date1 !== null && $request->date2 !== null)
        {
            $date1 = date('Y-m-d 00:00:00', strtotime($request->date1) );
            $date2 = date('Y-m-d 23:59:59', strtotime($request->date2) );
        }
        
        $sales = SalesDetail::leftJoin('sales', 'sales.sales_id', '=', 'sales_detail.sales_id')
                ->leftJoin('users', 'users.id', '=', 'sales.user_id')
                ->select('users.*', 'sales_detail.*', 'sales.created_at', 'sales.branch_code')
                ->where('sales.created_at', '>=', $date1)
                ->where('sales.created_at', '<=', $date2)
                ->where('sales_detail.is_return', 1)
                ->orderBy('sales.sales_id', 'desc')
                ->get();

        $no = 0;
        $data = array();

        foreach($sales as $list)
        {
            $return_reason = "";

            switch ($list->return_reason) {
                case 1:
                    $return_reason = "Replacement";
                    break;
                case 2:
                    $return_reason = "Canceled";
                    break;
                case 3:
                    $return_reason = "Damage";
                    break;
            }

            $no ++;
            $row = array();
            $row[] = $no;
            $row[] = date( "F j, Y, g:i a",strtotime($list->return_date));
            $row[] = $list->sales_id;
            $row[] = $list->return_qty;
            $row[] = "PhP. ".format_money(($list->return_qty * $list->price));
            $row[] = $list->branch_code;
            $row[] = $list->name;
            $row[] = $return_reason;
            $data[] = $row;
        }

        $output = array("data" => $data);
        return response()->json($output);
    }


    public function change()
    {

    }

    public function show($id)
    {
        $detail = SalesDetail::select('sales_detail.*','product.product_name')->leftJoin('product', 'product.product_code', '=', 'sales_detail.product_code')->where('sales_id', '=', $id)->get();
        $no = 0;
        $data = array();
        foreach($detail as $list)
        {
            $deduction = $list->return_qty * $list->price;
            $subtotal = $list->sub_total - $deduction;
            $no ++;
            $row = array();
            $row[] = $no;
            $row[] = $list->product_code;
            $row[] = $list->product_name;
            $row[] = "PhP. ".format_money($list->price);
            $row[] = ($list->is_return) ? "$list->return_qty <br><small class='text-warning'>Returned <b>$list->return_qty</b> items</small>" : $list->total;
            $row[] = "PhP. ".format_money($list->sub_total). (($list->is_return) ? "<br><small class='text-warning'>- $deduction = <b>PhP. $subtotal</b></small>" : "");
            $data[] = $row;
        }
    
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function destroy($id)
    {
        $sales = Sales::find($id);
        $sales->delete();

        $detail = SalesDetail::where('sales_id', '=', $id)->get();
        foreach($detail as $data){
            $product = Inventory::where('product_code', '=', $data->product_code)->where('branch_code', env('BRANCH_CODE'))->first();
            $product->stock += $data->quantity;
            $product->update();
            $data->delete();
        }
    }
}
