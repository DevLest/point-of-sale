<?php

namespace App\Http\Controllers;

use Datatables;
use App\Expenditure;
use Illuminate\Http\Request;

class ExpenditureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('expenditure.index'); 
    }

    public function listData()
    {
    
        $expenditure = Expenditure::orderBy('id_expenditure', 'desc')->get();
        $no = 0;
        $data = array();
        foreach($expenditure as $list){
            $no ++;
            $row = array();
            $row[] = $no;
            $row[] = tanggal_indonesia(substr($list->created_at, 0, 10), false);
            $row[] = $list->type_expenditure;
            $row[] = "Rp. ".format_uang($list->denomination);
            $row[] = '<div class="btn-group">
                        <a onclick="editForm('.$list->id_expenditure.')" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                        <a onclick="deleteData('.$list->id_expenditure.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a></div>';
            $data[] = $row;
        }

        $output = array("data" => $data);
        return response()->json($output);  
        // return Datatables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $expenditure = new Expenditure;
        $expenditure->expenditure_type   = $request['jenis'];
        $expenditure->denomination = $request['denomination'];
        $expenditure->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\expenditure  $expenditure
     * @return \Illuminate\Http\Response
     */
    public function show(Expenditure $expenditure)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\expenditure  $expenditure
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expenditure = Expenditure::find($id);
        echo json_encode($expenditure);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\expenditure  $expenditure
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $expenditure = Expenditure::find($id);
        $expenditure->type_expenditure   = $request['type'];
        $expenditure->denomination = $request['denomination'];
        $expenditure->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\expenditure  $expenditure
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expenditure = Expenditure::find($id);
        $expenditure->delete();
    }
}
