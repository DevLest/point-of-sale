<?php

namespace App\Http\Controllers;

use Redirect;
use Auth;
use PDF;
use Illuminate\Support\Facades\Hash;
use App\Sales;
use App\Product;
use App\Inventory;
use App\Member;
use App\Setting;
use App\SalesDetail;
use App\User;
use App\itemFormat;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\CapabilityProfile;

class SalesDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::select('product.*','inventory.*')->leftJoin('inventory', 'product.product_code', '=', 'inventory.product_code')->get();
        $member = Member::all();
        $setting = Setting::first();

        $returns = Sales::select('sales.sales_id', 'sales_detail.return_qty', 'sales_detail.price')->leftJoin('sales_detail', 'sales_detail.sales_id', '=', 'sales.sales_id')->where([ 'sales_detail.is_return' => 1, 'sales_detail.cleared_return' => 0 ])->get();
        $credit = 0;

        if (count($returns) > 0) 
        {
            $data = [];
            foreach ($returns as $return)
            {
                $data[$return->sales_id] = (isset($data[$return->sales_id])) ? $data[$return->sales_id] + ($return->price * $return->return_qty) : ($return->price * $return->return_qty);
            }

            $id = key($data);
            $credit = reset($data);
            session(['idreturn' => $id]);
            session(['returnCredit' => $credit]);
        }

        if(!empty(session('idsales')) || !empty(session('idreturn')))
        {
            $idsales = session('idsales');
            $idreturn = ( session('idreturn') != null) ? 1 : 0 ;
            return view('sales_detail.index', compact('product', 'member', 'setting', 'idsales', 'idreturn', 'credit'));
        }else{
            return Redirect::route('home');  
        }
    }

    public function listData($id)
    {
        $detail = SalesDetail::select('sales_detail.*','product.product_name', 'inventory.wholesaleprice')->leftJoin('product', 'product.product_code', '=', 'sales_detail.product_code')->leftJoin('inventory', 'product.product_code', '=', 'inventory.product_code')->where('sales_detail.sales_id', '=', $id)->orderBy('sales_detail.created_at','DESC')->get();
            
        $no = 0;
        $data = array();
        $total = 0;
        $total_item = 0;

        foreach($detail as $list)
        {
            $no ++;
            $checkStatus = ($list->is_whole_sale) ? "checked":"";
            $total += ( $list->price * $list->total ) - $list->discount;
            $total_item += $list->total;
            
            $row = array();

            $row[] = $no;
            $row[] = $list->product_code;
            $row[] = $list->product_name;
            $row[] = "₱ <input type='number' hidden name='price_$list->sales_detail_id' id='price_$list->sales_detail_id' value='$list->price''>".format_money($list->price);
            $row[] = "<input type='number' class='form-control' name='total_$list->sales_detail_id' value='$list->total' id='total_$list->sales_detail_id'  onChange='changeCount($list->sales_detail_id)'>";
            $row[] = "<input type='text' class='form-control' step='.01' name='discount_$list->sales_detail_id' id='discount_$list->sales_detail_id' value='$list->discount' onClick='changeDiscount($list->sales_detail_id)' readonly> <input type='hidden' class='wholesale_$list->sales_detail_id' value='$list->wholesaleprice'>";
            $row[] = "₱ ".format_money($list->sub_total);
            $row[] = '  <div class="form-group">
                            <label class="switch col-sm-2" onclick="wholesaleprice('.$list->sales_detail_id.')" id="whole'.$list->sales_detail_id.'" data-toggle="tooltip" title="Enable Wholesale Price">
                                <input type="checkbox" id="wholesale'.$list->sales_detail_id.'" '.$checkStatus.'>
                                <span class="slider round"></span>
                            </label>
                            <div class="btn-group col-sm-6">
                                    <a onclick="deleteItem('.$list->sales_detail_id.')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Remove Product"><i class="fa fa-trash"></i></a>
                        </div>';
            $data[] = $row;
        }

        $data[] = array("<span class='hide total'>$total</span><span class='hide totalitem'>$total_item</span>", "", "", "", "", "", "", "");
        
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        $product = Inventory::where('product_code', $request['code'])->first();

        if ( $product )
        {
            $detail = SalesDetail::where(['sales_id' => $request['idsales'], 'product_code' => $request['code'] ])->first();
            
            if ( $detail )
            {
                $detail->total += 1;
                $detail->sub_total = ( $detail->total * $product->price ) - $detail->discount;
            }
            else
            {
                $detail = new SalesDetail;
                $detail->sales_id = $request['idsales'];
                $detail->product_code = $request['code'];
                $detail->price = $product->price;
                $detail->total = 1;
                $detail->cost = $product->cost;
                $detail->discount = $product->discount;
                $detail->sub_total = $product->price - $detail->discount;
            }

            if($detail->save())
            {
                return response("Good");
            }
            return response("Bad");
        }
        else
        {
            return response("Bad");
        }
    }

    public function show(SalesDetail $salesDetail)
    {
    }

    public function edit(SalesDetail $SalesDetail)
    {
    }

    public function update(Request $request, $id)
    {
        $input_name = "total_".$id;
        $detail = SalesDetail::find($id);
        $total_price = $request[$input_name] * $detail->price;

        $detail->total = $request[$input_name];
        $detail->sub_total = $total_price - $detail->discount;
        $detail->update();
    }

    public function destroy($id)
    {
        $detail = SalesDetail::find($id);
        $detail->delete();
    }

    public function newSession()
    {
        if (!empty(session('idsales')))
        {
            $sales = Sales::where('sales_id', session('idsales'))->first();
            if (!$sales || ($sales->pay > 0) )
            {
                $sales = new Sales; 
                $sales->member_code = 0;    
                $sales->total_item = 0;    
                $sales->total_price = 0;    
                $sales->discount = 0;    
                $sales->pay = 0;    
                $sales->received = 0;    
                $sales->user_id = Auth::user()->id;    
                $sales->branch_code = env('BRANCH_CODE');    
                $sales->save();
            }
        }
        else
        {
            $sales = new Sales; 
            $sales->member_code = 0;    
            $sales->total_item = 0;    
            $sales->total_price = 0;    
            $sales->discount = 0;    
            $sales->pay = 0;    
            $sales->received = 0;    
            $sales->user_id = Auth::user()->id;    
            $sales->branch_code = env('BRANCH_CODE');    
            $sales->save();
        }
        
        session(['idsales' => $sales->sales_id]);

        return Redirect::route('transaction.index');    
    }

    public function saveData(Request $request)
    {
        if ( isset( $request->received ) && $request->received > 0 ) {
            $sales = Sales::find($request['idsales']);
            if ($sales)
            {
                if (session('idreturn') != null && !empty(session('idreturn')) )
                {
                    $returnDetails = SalesDetail::where(['sales_id' => session('idreturn'), 'is_return' => 1 ])->get();
                    foreach ($returnDetails as $return)
                    {
                        $return->cleared_return = 1;
                        $return->save();
                    }

                    $sales->discount = ( isset($request['discount']) ) ? $request['discount'] : session('returnCredit');
                    $sales->linked_return = session('idreturn');

                    session()->forget('idreturn');                
                }

                $sales->member_code = ( isset($request['member']) ) ?$request['member'] : 0;
                $sales->total_item = $request['totalitem'];
                $sales->total_price = $request['total'];
                $sales->pay = $request['pay'];
                $sales->received = $request['received'];

                $detail = SalesDetail::where('sales_id', '=', $request['idsales'])->get();

                $net = 0;
                foreach($detail as $data)
                {
                    $product = Inventory::where('product_code', '=', $data->product_code)->first();
                    if ($product)
                    {
                        $product->stock -= $data->total;
                        $product->update();
                        $net += ( ( $product->price * $data->total ) - ( $product->cost * $data->total ) );
                    }
                }

                $sales->net = $net;
                $sales->update();
            }
            // return Redirect::route('transaction.print');
            return Redirect::route('transaction.pdf');
        }
        return Redirect::back();
    }
    
    public function loadForm($discount, $total, $received, $id)
    {
        if ( $id > 0 )
        {
            $salesDetail = SalesDetail::find($id);
            $salesDetail->discount = $discount;
            $salesDetail->sub_total = ( $salesDetail->price * $salesDetail->total ) - (float) $discount;
            $salesDetail->save();
        }

        $pay = $total - $discount;
        $return = $received - $pay;

        $data = array(
            "totalrp" => format_money($pay),
            "pay" => $pay,
            "payrp" => format_money($pay),
            "number" => (($pay > 0) ? ucwords( numberTowords( $pay ) ) : "" )." PESOS",
            "returnirp" => format_money($return),
            "returnnumber" => (($received <= 0) ? ucwords( numberTowords( $pay ) ) : ucwords( numberTowords( ($return < 0 ) ? 0 : $return ) ) )." PESOS"
        );
        return response()->json($data);
    }

    public function printNote()
    {
        $detail = SalesDetail::leftJoin('product', 'product.product_code', '=', 'sales_detail.product_code')
            ->where('sales_detail.sales_detail_id', '=', session('idsales'))
            ->get();

        $sales = Sales::find(session('idsales'));
        $setting = Setting::find(1);
        
        if($setting->note_type == 0){
            $handle = \printer_open(); 
            \printer_start_doc($handle, "Note");
            \printer_start_page($handle);

            $font = \printer_create_font("Consoles", 100, 80, 600, false, false, false, 0);
            \printer_select_font($handle, $font);
            
            \printer_draw_text($handle, $setting->nama_perusahaan, 400, 100);

            $font = \printer_create_font("Consoles", 72, 48, 400, false, false, false, 0);
            \printer_select_font($handle, $font);
            \printer_draw_text($handle, $setting->address, 50, 200);

            \printer_draw_text($handle, date('Y-m-d'), 0, 400);
            \printer_draw_text($handle, substr("             ".Auth::user()->name, -15), 600, 400);

            \printer_draw_text($handle, "No : ".substr("00000000".$sales->id, -8), 0, 500);

            \printer_draw_text($handle, "============================", 0, 600);
            
            $y = 700;
            
            foreach($detail as $list){           
                \printer_draw_text($handle, $list->product_code." ".$list->product_name, 0, $y+=100);
                \printer_draw_text($handle, $list->total." x ".format_money($list->price), 0, $y+=100);
                \printer_draw_text($handle, substr("                ".format_money($list->price*$list->total), -10), 850, $y);

                if($list->discount != 0){
                    \printer_draw_text($handle, "Diskon", 0, $y+=100);
                    \printer_draw_text($handle, substr("                      -".format_money($list->discount/100*$list->sub_total), -10),  850, $y);
                }
            }
            
            \printer_draw_text($handle, "----------------------------", 0, $y+=100);

            \printer_draw_text($handle, "Total Amount: ", 0, $y+=100);
            \printer_draw_text($handle, substr("           ".format_money($sales->total+price), -10), 850, $y);

            \printer_draw_text($handle, "Total Item: ", 0, $y+=100);
            \printer_draw_text($handle, substr("           ".$sales->total_item, -10), 850, $y);

            \printer_draw_text($handle, "Member Discount: ", 0, $y+=100);
            \printer_draw_text($handle, substr("           ".$sales->discount."%", -10), 850, $y);

            \printer_draw_text($handle, "Total Payment: ", 0, $y+=100);
            \printer_draw_text($handle, substr("            ".format_money($sales->pay), -10), 850, $y);

            \printer_draw_text($handle, "Cash Received: ", 0, $y+=100);
            \printer_draw_text($handle, substr("            ".format_money($sales->received), -10), 850, $y);

            \printer_draw_text($handle, "Change: ", 0, $y+=100);
            \printer_draw_text($handle, substr("            ".format_money($sales->received-$sales->pay), -10), 850, $y);

            \printer_draw_text($handle, "============================", 0, $y+=100);
            \printer_draw_text($handle, "-= Thank you for Shopping =-", 250, $y+=100);
            \printer_delete_font($font);
            
            \printer_end_page($handle);
            \printer_end_doc($handle);
            \printer_close($handle);
        }
        
        return view('sales_detail.done', compact('setting'));
    }

    public function notePDF()
    {
        $detail = SalesDetail::select('sales_detail.*','product.product_name')->leftJoin('product', 'product.product_code', '=', 'sales_detail.product_code')
            ->where('sales_detail.sales_id', '=', session('idsales'))
            ->get();

        $sales = Sales::select('sales.*', 'users.name as username')->leftJoin('users', 'users.id', '=', 'sales.user_id')->where('sales_id',session('idsales'))->first();
        $setting = Setting::find(1);
        $no = 0;
        
        // $pdf = PDF::loadView('sales_detail.notepdf', compact('detail', 'sales', 'setting', 'no'));
        // $pdf->setPaper(array(0,0,550,440), 'potrait');      
        // return $pdf->stream();

        try 
        {
            // $connector = new WindowsPrintConnector("smb://".gethostname()."/".env('PRINTER_NAME'));
            // $profile = CapabilityProfile::load("simple");
            // $printer = new Printer($connector, $profile);
    
            // $printer->setJustification(Printer::JUSTIFY_CENTER);
    
            // /* Name of shop */
            // $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
            // $printer->text(strtoupper($setting->store_name)."\n");
            // $printer->selectPrintMode();
            // $printer->text("$setting->address\n");
            
            // /* Title of receipt */
            // $printer->setEmphasis(true);
            // $printer->text("Order Form\n");
            // $printer->setEmphasis(false);
            
            // /* Order Number*/
            // $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
            // $printer->text("$sales->sales_id\n");
            // $printer->selectPrintMode();
            // $printer->feed();
            
            // /* Items */
            // $printer->setJustification(Printer::JUSTIFY_LEFT);
            // $printer->setEmphasis(true);
            // $printer->text(new itemFormat('QTY | Product Name', '$'));
            // $printer->setEmphasis(false);
    
            // foreach ($detail as $item) 
            // {
            //     $product = Product::where('product_code', $item->product_code)->first();
            //     $product = ($product) ? $product->product_name : $item->product_code;
    
            //     $printer->text( new itemFormat("$item->total | ".substr($product, 0 , 21), $item->sub_total));
            // }
    
            // $printer->feed();
            // $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
            // $printer->text(new itemFormat('Total Amount', $sales->pay));
            // $printer->selectPrintMode();
    
            // $printer->text(new itemFormat('Cash Receive', $sales->received));
    
            // $printer->setEmphasis(true);
            // $printer->text(new itemFormat('Change', ($sales->received - $sales->pay)));
            // $printer->setEmphasis(false);
            
            // $printer->feed(2);
            // $printer->setJustification(Printer::JUSTIFY_CENTER);
            // $printer->text("Thank you for choosing $setting->store_name\n");
            // $printer->text("For store hours, please visit $setting->store_name Facebook page\n");
            // $printer->feed();
            // $printer->text(date( 'l jS \of F Y h:i:s A' ,time()) . "\n");
            // $printer->feed();
            // $printer->text(new itemFormat('Teller', $sales->username));
            // $printer->feed(3);
    
            // $printer->cut();
            // $printer->pulse();
            // $printer->close();
        }
        catch (Exception $e){}
        
        session()->forget('idsales');
        return view('sales_detail.notepdf', compact('detail', 'sales', 'setting', 'no'));
    }

    public function wholesale(Request $request)
    {
        $detail = SalesDetail::find($request->id);

        if ($detail)
        {
            $product = Inventory::where('product_code', $detail->product_code)->first();
            
            if ($product)
            {
                $detail->is_whole_sale = ($detail->is_whole_sale) ? 0 : 1;
                $detail->price = ($detail->is_whole_sale) ? ($product->wholesaleprice > 0) ? $product->wholesaleprice : $product->price : $product->price;
                $detail->sub_total = ( $detail->price * $detail->total ) - $detail->discount;
                $detail->save();
            }
        }
    }

    public function admincheck(Request $request)
    {
        $admun = User::where('level', 1)->get();
        $approve = 0;
        
        foreach ($admun as $user)
        {
            if (Hash::check($request->password, $user->password))
            {
                $approve = 1;
                break;
            }
        }
        
        if ($approve)  return response()->json(['status'=>'true','message'=>'Login approve']);

        return response()->json(['status'=>'false','message'=>'Action disapprove! Credentials not correct']);
    }

    public function return()
    {
        if ( session('return_id') == null ) session(['return_id' => 0]);
        return view('sales_detail.return');
    }

    public function returnConfirm(Request $request)
    {
        $checkedReturns = [];
        foreach ($request->all() as $key => $value) 
        {
            if ( strpos($key, 'check_') === 0 && $value === 'on' ) 
            {
                $checkedReturns[] = str_replace('check_', '', $key);
            }
        }

        $orderItems = SalesDetail::select('*')->whereIn('sales_detail_id', $checkedReturns)->get();

        $output = '<table class="table table-striped table-sales table-responsive">
        <thead>
            <tr>
                <th>Product Code</th>
                <th align="right">Sold Price</th>
                <th>Sold Quantity</th>
                <th>Return Quantity</th>
                <th>Return Reason</th>
            </tr>
        </thead><tbody>';

        foreach($orderItems as $items)
        {
            $output .= "<tr>
                <td>$items->product_code</td>
                <td>$items->price</td>
                <td>$items->total</td>
                <td><input type='number' min='1' name='$items->sales_detail_id' max='$items->total' value='$items->total' /></td>
                <td>
                    <select class='form-select' aria-label='Return type' name='returnReason$items->sales_detail_id'> 
                        <option value='1' selected>Replacement</option>
                        <option value='2'>Canceled</option>
                        <option value='3'>Damage</option>
                    </select>
                </td>
            </tr>";
        }
        
        $output .= "
            </tbody>
        </table>
        ";

        echo $output;
    }

    public function returnID($id)
    {
        $sales = Sales::where('sales.sales_id',$id)->join('sales_detail', 'sales_detail.sales_id', '=', 'sales.sales_id')->first();

        if ($sales)
        {
            if (Carbon::createFromFormat('Y-m-d H:i:s', $sales->created_at)->format('Y-m-d H:i:s') >= date('Y-m-d H:i:s', strtotime('-3 day')) )
            {
                session(['return_id' => $sales->sales_id]);
                
                return response()->json(['status'=>'true','message'=>'']);
            }
            else
            {
                return response()->json(['status'=>'false','message'=>"Can't return transactions after 3 days"]);
            }
        }
        
        return response()->json(['status'=>'false','message'=>'No transaction found! Please check OR number']);
    }

    public function returnData($id)
    {
        $detail = SalesDetail::select('sales_detail.*','product.product_name')->leftJoin('product', 'product.product_code', '=', 'sales_detail.product_code')->where('sales_detail.sales_id', '=', $id)->orderBy('sales_detail.sales_id')->get();
            
        $no = 0;
        $data = array();
        $total = 0;
        $total_item = 0;

        foreach($detail as $list)
        {
            if ($list->total > 0)
            {
                $no ++;
                $checkStatus = ($list->is_whole_sale) ? "checked":"";
                $total += ( $list->price * $list->total ) - $list->discount;
                $total_item += $list->total;
                
                $row = array();
    
                $row[] = $no;
                $row[] = $list->product_code;
                $row[] = $list->product_name;
                $row[] = "₱ ".$list->price;
                $row[] = $list->total;
                $row[] = "₱ ".$list->discount;
                $row[] = "₱ ".format_money($list->sub_total);
                $row[] = '<input type="checkbox" aria-label="Check if items is received upon return" id="check_'.$list->sales_id.'" name = "check_'.$list->sales_detail_id.'">
                <label class="form-check-label" for=""check_'.$list->sales_detail_id.'"">
                  Return
                </label>';
                $data[] = $row;
            }
        }

        $data[] = array("<span class='hide total'>$total</span><span class='hide totalitem'>$total_item</span>", "", "", "", "", "", "", "");
        
        $output = array("data" => $data);
        return response()->json($output);
    }

    public function returnSubmit(Request $request)
    {
        $parameters = $request->all();
        unset($parameters['_token']);
        
        $reasons = [];
        $ids = [];
        $qty_value = [];
        foreach ($parameters as $key => $value) 
        {
            if ( strpos($key, 'returnReason') === 0) 
            {
                $id = str_replace('returnReason', '', $key);
                $reasons[$id] = $value;
            }
            else
            {
                $ids[] = (int) $key;
                $qty_value[$key] = $value;
            }
        }

        $orderItems = SalesDetail::select('*')->whereIn('sales_detail_id', $ids)->get();
        
        foreach ($orderItems as $orderItem) 
        {
            $qty = $qty_value[$orderItem->sales_detail_id];
            $orderItem->is_return = 1;
            $orderItem->return_qty = $qty;
            $orderItem->total = $orderItem->total - $qty;
            $orderItem->return_reason = $reasons[$orderItem->sales_detail_id];
            $orderItem->return_date = date('Y-m-d', time());

            if ( $orderItem->save() )
            {
                \DB::table('sales')->where('sales_id', $orderItem->sales_id)->update(['has_return' => 1]);

                if ( $orderItem->return_reason !== 3 )
                {
                    $product = Inventory::where('product_code', '=', $orderItem->product_code)->first();
                    $product->stock += $qty;
    
                    $product->save();
                }

                session(['idreturn' => $orderItem->sales_id]);
            }
        }

        return redirect()->route('transaction.new');
    }

    public function addCustomProduct(Request $request)
    {
        $product_code = strtoupper($request['chargeType'])." | ".$request['product_name'];

        $detail = SalesDetail::where(['sales_id' => $request['idsales'], 'product_code' => $product_code ])->first();
        
        if ( $detail )
        {
            $detail->total += 1;
            $detail->sub_total = $request['product_quantity'] * $request['product_price'];
        }
        else
        {
            $detail = new SalesDetail;
            $detail->sales_id = $request['idsales'];
            $detail->product_code = $product_code;
            $detail->price = $request['product_price'];
            $detail->total = $request['product_quantity'];
            $detail->discount = 0;
            $detail->cost = 0;
            $detail->sub_total = $request['product_quantity'] * $request['product_price'];
        }

        $detail->save();

        return redirect()->back();
    }
}
