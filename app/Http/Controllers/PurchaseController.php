<?php

namespace App\Http\Controllers;

use Redirect;
use App\Supplier;
use App\PurchaseDetail;
use App\Product;
use App\Purchase;
use Illuminate\Http\Request;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supplier = Supplier::all();
        return view('purchase.index', compact('supplier'));       
    }

    public function listData()
    {
    
        $purchase = Purchase::leftJoin('supplier', 'supplier.supplier_id', '=', 'purchase.supplier_id')
            ->orderBy('purchase.purchase_id', 'desc')
            ->get();
        $no = 0;
        $data = array();
        foreach($purchase as $list){
            $no ++;
            $row = array();
            $row[] = $no;
            $row[] = tanggal_indonesia(substr($list->created_at, 0, 10), false);
            $row[] = $list->suplier_name;
            $row[] = $list->quantity;
            $row[] = "PhP. ".format_money($list->total_amount);
            $row[] = $list->diskon."%";
            $row[] = "PhP. ".format_money($list->payment);
            $row[] = '<div class="btn-group">
                    <a onclick="showDetail('.$list->purchase_id.')" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                    <a onclick="deleteData('.$list->purchase_id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                </div>';
            $data[] = $row;
        }

        $output = array("data" => $data);
        return response()->json($output);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $purchase = new Purchase;
        $purchase->supplier_id = $id;     
        $purchase->quantity = 0;     
        $purchase->total_amount = 0;     
        $purchase->discount = 0;     
        $purchase->payment = 0;     
        $purchase->save();

        session(['sales_id' => $purchase->purchase_id]);
        session(['supplier_id' => $id]);

        return Redirect::route('purchase_detail.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $purchase = Purchase::find($request['idpurchase']);
        $purchase->total_item = $request['totalitem'];
        $purchase->total_amount = $request['total'];
        $purchase->discount = $request['discount'];
        $purchase->pay = $request['pay'];
        $purchase->update();

        $detail = PurchaseDetail::where('id_purchase', '=', $request['idpurchase'])->get();
        foreach($detail as $data){
            $produk = Produk::where('kode_produk', '=', $data->kode_produk)->first();
            $produk->stok += $data->jumlah;
            $produk->update();
        }
        return Redirect::route('purchase.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pembelian  $pembelian
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = PurchaseDetail::leftJoin('produk', 'produk.kode_produk', '=', 'purchase_detail.kode_produk')
        ->where('id_purchase', '=', $id)
        ->get();
        $no = 0;
        $data = array();
        foreach($detail as $list){
            $no ++;
            $row = array();
            $row[] = $no;
            $row[] = $list->kode_produk;
            $row[] = $list->nama_produk;
            $row[] = "Rp. ".format_uang($list->harga_beli);
            $row[] = $list->jumlah;
            $row[] = "Rp. ".format_uang($list->harga_beli * $list->jumlah);
            $data[] = $row;
        }
        
        $output = array("data" => $data);
        return response()->json($output);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pembelian  $pembelian
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pembelian  $pembelian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pembelian  $pembelian
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $purchase = Purchase::find($id);
        $purchase->delete();

        $detail = PurchaseDetail::where('id_purchase', '=', $id)->get();
        foreach($detail as $data){
            $produk = Produk::where('kode_produk', '=', $data->kode_produk)->first();
            $produk->stok -= $data->jumlah;
            $produk->update();
            $data->delete();
        }
    }
}
