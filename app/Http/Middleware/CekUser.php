<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth as Auth;
use Closure;

class CekUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $level)
    {
        $user = Auth::user();
        
        if ($user == null) {
            return redirect('login');
        }

        // if($user && $user->level != $level){
        if($user && $user->level == 2){
            return redirect('/');
        }
        else return $next($request);
    }
}
