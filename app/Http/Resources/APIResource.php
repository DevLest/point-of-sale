<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class APIResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'productName' => $this->product_name,
            'cost' => $this->cost,
            'price' => $this->price,
        ];
    }

    public function with($request)
    {
        return [
            'version' => "1.0.0",
            'author_url' => url('http://pointofsale.test'),
        ];
    }
}
