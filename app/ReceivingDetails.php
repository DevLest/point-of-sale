<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceivingDetails extends Model
{
    protected $table = 'receive_details';
    protected $primaryKey = 'id';
    public $timestamp = true;
}
