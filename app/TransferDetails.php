<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransferDetails extends Model
{
    protected $table = 'transfer_details';
    protected $primaryKey = 'id';
    public $timestamp = true;
}
