<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTabelSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('setting', function(Blueprint $table){
         $table->increments('id_setting');       
         $table->string('store_name', 100);       
         $table->text('address');       
         $table->string('phone', 20);           
         $table->string('logo', 50);                
         $table->string('card_member', 50);                
         $table->integer('discount_member')->unsigned();                
         $table->integer('note_type')->unsigned();                
         $table->timestamps();      
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('setting');
    }
}
