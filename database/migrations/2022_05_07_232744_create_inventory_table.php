<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('product_code', 100);       
            $table->bigInteger('cost')->unsigned();
            $table->integer('discount')->unsigned()->nullable()->default(0);             
            $table->bigInteger('price')->unsigned();
            $table->bigInteger('wholesaleprice')->unsigned();
            $table->string('stock',50)->nullable()->default(0);
            $table->string('branch_code', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory');
    }
}
