<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMember extends Migration
{
   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
      Schema::create('member', function(Blueprint $table){
         $table->increments('member_id');        
         $table->biginteger('member_code')->unsigned();       
         $table->string('name', 100);         
         $table->text('address');         
         $table->string('contact', 20);      
         $table->timestamps();       
      });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {      
      Schema::drop('member');
   }
}
