<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRetrunToSalesDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_detail', function (Blueprint $table) {
            $table->integer('is_return')->nullable()->default(0);
            $table->integer('return_qty')->nullable()->default(0);
            $table->longText('reason_for_return')->nullable();
            $table->date('return_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_detail', function (Blueprint $table) {
            $table->dropColumn('is_return');
            $table->dropColumn('return_qty');
            $table->dropColumn('reason_for_return');
            $table->dropColumn('return_date');
        });
    }
}
