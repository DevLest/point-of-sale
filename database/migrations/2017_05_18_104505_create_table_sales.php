<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSales extends Migration
{
   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
      Schema::create('sales', function(Blueprint $table){
         $table->increments('sales_id');        
         $table->bigInteger('member_code')->unsigned();            
         $table->integer('total_item')->unsigned();         
         $table->bigInteger('total_price')->unsigned();           
         $table->integer('discount')->unsigned();       
         $table->bigInteger('pay')->unsigned();     
         $table->bigInteger('received')->unsigned();     
         $table->integer('user_id')->unsigned();     
         $table->timestamps();       
      });

      DB::statement("ALTER TABLE sales AUTO_INCREMENT = ".env('SALES_COUNT').";");
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
      Schema::drop('sales');
   }
}
