<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function(Blueprint $table){
            $table->increments('product_id');          
            $table->string('product_code', 100);
            $table->integer('category_id')->unsigned();           
            $table->longText('product_name');           
            $table->string('brand', 50)->nullable();    
            $table->timestamps();         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {        
        Schema::drop('product');
    }
}
