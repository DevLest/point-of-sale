<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSalesDetail extends Migration
{
   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
      Schema::create('sales_detail', function(Blueprint $table){
         $table->increments('sales_detail_id');       
         $table->integer('sales_id')->unsigned();
         $table->string('product_code', 100);
         $table->bigInteger('price')->unsigned();
         $table->integer('total')->unsigned();
         $table->integer('discount')->unsigned();
         $table->bigInteger('sub_total')->unsigned();
         $table->integer('is_whole_sale')->unsigned()->default(0);     
         $table->timestamps();      
      });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
      Schema::drop('sales_detail');
   }
}
