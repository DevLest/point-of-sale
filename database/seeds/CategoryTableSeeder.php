<?php

use Illuminate\Database\Seeder;
use DB;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i <= 100; $i++) { 
            DB::table('category')->insert(array(
                [
                    'category' => 'Goods '.$i
                ]
            ));
        };
    }
}
