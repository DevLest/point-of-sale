<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            [
                'name' => 'Admin', 
                'email' => 'admin',
                'password' => bcrypt('admin'),
                'foto' => 'user.png',
                'level' => 1
            ],
            [
                'name' => 'Super Admin', 
                'email' => 'superad',
                'password' => bcrypt('superad'),
                'foto' => 'user.png',
                'level' => 1
            ],
            [
                'name' => 'user 1', 
                'email' => 'user',
                'password' => bcrypt('user'),
                'foto' => 'user.png',
                'level' => 2
            ]
        ));
    
    }
}
