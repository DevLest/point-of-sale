<?php

use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('setting')->insert(array(
            [
                'store_name' => 'Pets Office', 
                'address' => 'Bacolod City',
                'phone' => '4335362',
                'logo' => 'logo.jpg',
                'card_member' => 'card.png',
                'discount_member' => '10',
                'note_type' => '0',
                'branch_code' => env('BRANCH_CODE'),
            ]
        ));
    }
}
