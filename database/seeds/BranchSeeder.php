<?php

use Illuminate\Database\Seeder;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('branch')->insert(array(
            [
                'name' => 'Central Warehouse', 
                'location' => 'Bacolod City',
                'branch_code' => 'MAIN1',
            ],
            [
                'name' => 'Pets Office Burgus', 
                'location' => 'Luzuriaga St., Bacolod City',
                'branch_code' => 'BCD1',
            ],
            [
                'name' => 'Pets Office Libertad', 
                'location' => 'Libertad, Bacolod City',
                'branch_code' => 'BCD2',
            ],
            [
                'name' => 'Pets Box', 
                'location' => 'Binalbagan',
                'branch_code' => 'BIN1',
            ],
        ));
    }
}
